#pragma once

#include "SDL.h"
#include "SDL_mixer.h"
#include "MACRO_MOSTRAR.h"
//#define MOSTRAR_SONIDO 1

class AudioGeneral
{
private:
	unsigned int error;
	unsigned int frecuencia;
	unsigned int canales;
public:
	AudioGeneral();
	~AudioGeneral();
	unsigned int getError() { return error; }
};

