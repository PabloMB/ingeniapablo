#pragma once

//para c�lculos
#define PI 3.14159265358979323846

#define G -9.81

//de la partida
#define TOTAL_TIME 180 //tiempo de duraci�n de la partida en segundos

//para la c�mara
#define CAM_DIST 7
#define CAM_DIST_VAR 0.51 //0.01 para evitar que distancia(Yo-Camara) d� nula
#define CAM_ANG 15 //grados
#define SCROLL_TIMES0 5
#define SCROLL_TIMES_MAX 100

//para el humano
#define PENDIENTE_MAX 1.0
#define YO_X0 -190.0
#define YO_Y0 -70.0
#define YO_Z0 0 //esta no importa ya que se recalcular�
#define YO_DIRECCION0 -90.0
#define YO_X1 -169.0 //X1 est� siendo usado por osg
#define YO_Y1 158.0
#define YO_Z1 0 //esta no importa ya que se recalcular�
#define YO_DIRECCION1 0.0
#define XX2 183.0
#define YY2 208.0
#define ZZ2 0 //esta no importa ya que se recalcular�
#define DIRECCION2 0.0
#define CTE_GIRO 0.05
#define CTE_AVANCE 0.2
#define VEL_ANDAR 5
#define DESNIVEL 0.1
#define DISTANCIA_MIN_A_OTROS 3.0

//para la casa
#define CASA_X0 -170.0
#define CASA_Y0 160.0
#define CASA_Z0 160.0
#define CASA_AJUSTEX -170.0
#define CASA_AJUSTEY -170.0
#define CASA_AJUSTEZ 160.0

//para el avi�n
#define AVION_X0 -200.0
#define AVION_Y0 -90.0
#define AVION_Z0 0 //esta no importa ya que se recalcular�
#define AVION_ANGX0 -10.0 //giro entorno al eje x
#define AVION_ANGY0 -10.0
#define AVION_ANGZ0 -45.0
#define AVION_BAJARZ 15.0

//para el heightmap
#define HEIGHTMAP_ARCHIVO "./Heightmap/Texturas/Map3.png"//Heightmap.png"
#define HEIGHTMAP_ARCHIVO_NORMALES "./Heightmap/Texturas/Map3_n.png"//heightmap_normals.png"
#define HEIGHTMAP_ARCHIVO_TEXTURAS "./Heightmap/Texturas/Map3_c.png"//heightmap_rgb.png"
#define HEIGHTMAP_TEXTURA_CESPED "./Heightmap/Texturas/cesped.dds"
#define HEIGHTMAP_TEXTURA_NIEVE "./Heightmap/Texturas/snow.dds"
#define HEIGHTMAP_TEXTURA_MONTA�A "./Heightmap/Texturas/mountain.dds"
#define HEIGHTMAP_TEXTURA_DESIERTO "./Heightmap/Texturas/desert.dds"
#define HEIGHTMAP_TEXTURA_TIERRA "./Heightmap/Texturas/tierra2.jpg"
#define HEIGHTMAP_ARCHIVO_VERT "./Heightmap/heightmap_sombras.vert"
#define HEIGHTMAP_ARCHIVO_FRAG "./Heightmap/heightmap_sombras.frag"
#define HEIGHTMAP_ALTURA 100
#define NUM_PUNTOS 512
#define SEPARACION_PUNTOS 1

//para los �rboles
#define XML_TREES "./Archivos/trees.xml"
#define XML_TREES2 "./Archivos/treesBUENO.xml"
//para las hierbas
#define XML_WEEDS "./Archivos/weeds1000.xml"


//para el mapa
#define MAPA_ARCHIVO "./Mapas/mapa4.png"
#define MAP_MAX_SIZE 400
#define MAP_MIN_SIZE 200
#define MAPA_ARCHIVO_BIS "./Mapas/mapa4bis.png"
#define MAPA_CURSOR "./Mapas/indice2.png"
#define CURSOR_HEIGHT 49
#define CURSOR_WIDTH 49

//para los objetos
#define OBJETO_MAPA "./Mapas/mapa3.png"
#define OBJETO_PAQUETE "./Texturas/Objetos/paquete.jpg"
#define OBJETO_PISTOLA "./Texturas/Objetos/pistola.jpg"
#define OBJETO_BALA "./Texturas/Objetos/bala.jpg"
#define OBJETO_LAPIZ "./Texturas/Objetos/lapiz.png"
#define OBJETO_3PUNTOS "./Texturas/Objetos/three_dots.png"
#define OBJETO_BLANCO "./Texturas/Objetos/blanco.png"

//para los numeros
#define NUMERO_0 "./Texturas/Numeros/numero0.jpg"
#define NUMERO_1 "./Texturas/Numeros/numero1.jpg"
#define NUMERO_2 "./Texturas/Numeros/numero2.jpg"
#define NUMERO_3 "./Texturas/Numeros/numero3.jpg"
#define NUMERO_4 "./Texturas/Numeros/numero4.jpg"
#define NUMERO_5 "./Texturas/Numeros/numero5.jpg"
#define NUMERO_6 "./Texturas/Numeros/numero6.jpg"
#define NUMERO_7 "./Texturas/Numeros/numero7.jpg"
#define NUMERO_8 "./Texturas/Numeros/numero8.jpg"
#define NUMERO_9 "./Texturas/Numeros/numero9.jpg"
#define NUMERO_P "./Texturas/Numeros/PuntosInicial.jpg"

//para el rayo
#define LONGITUD_RAYO 0.5

//para la trayectoria del sol
#define CENTRO_X 0.0
#define CENTRO_Y 0.0
#define CENTRO_Z 0.0
#define RADIO 1200.0
#define ANG_PHI0 25.0	//en grados
#define ANG_THETA0 0.0	//en grados
#define VEL_ANG_PHI 0.01
#define VEL_ANG_THETA 0.0

//para el WindowManager
const unsigned int MASK_2D = 0xF0000000;
const unsigned int MASK_3D = 0x0F000000;

//para los di�logos
#define MAX_POR_LINEA 45


