#include "TrayectoriaCircular.h"


TrayectoriaCircular::TrayectoriaCircular()
{
}

TrayectoriaCircular::TrayectoriaCircular(double a, double b, double c, double r, double p, double t)
{
	centro.setX(a);
	centro.setY(b);
	centro.setZ(c);
	radio = r;
	phi = p;
	theta = t;
}


TrayectoriaCircular::~TrayectoriaCircular()
{
}
