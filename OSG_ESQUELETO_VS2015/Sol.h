#pragma once

//#include "constantes.h"
#include "punto.h"
#include "TrayectoriaCircular.h"
#include <math.h>
#include <string>


class Solazo : public TrayectoriaCircular
{
	private:
		Punto posicion;
	public:
		Solazo(double a, double b, double c, double r, double p, double t);
		void move(double var_t);
		double getDayDuration();
		std::string tellDayDuration();
		double getX() { return posicion.getX(); };
		double getY() { return posicion.getY(); };
		double getZ() { return posicion.getZ(); };
};

