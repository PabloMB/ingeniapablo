#pragma once

#include <osg/Light>

osg::Light* crearLuzDeInterior(Punto p) {
	osg::Light* light = new osg::Light;

	light->setLightNum(1);
	light->setPosition(osg::Vec4(p.getX(), p.getY(), p.getZ(), 1.0f));
	light->setAmbient(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f));
	light->setDiffuse(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f));
	light->setSpotCutoff(20.0f);
	light->setSpotExponent(50.0f);
	light->setDirection(osg::Vec3(1.0f, 1.0f, -1.0f));
	light->setQuadraticAttenuation(1);

	return light;
}

osg::LightSource* crearLightSource(osg::Light* light) {
	osg::LightSource* lightSource = new osg::LightSource;
	lightSource->setLight(light);
	lightSource->setLocalStateSetModes(osg::StateAttribute::ON);
	return lightSource;
}

osg::ref_ptr<osgShadow::ShadowedScene> crearShadowedScene_paraLuzDeInterior(osg::Light* light) {
	const int ReceivesShadowTraversalMask = 0x1;
	const int CastsShadowTraversalMask = 0x2;
	const int ReceivesAndCastsShadowTraversalMask = 0x3;
	float minLightMargin = 10.f;
	float maxFarPlane = 1000;
	unsigned int texSize = 1024;
	unsigned int baseTexUnit = 0;
	unsigned int shadowTexUnit = 7;
	texSize = 4096;

	osg::ref_ptr<osgShadow::ShadowedScene> ss = new  osgShadow::ShadowedScene;
	osg::ref_ptr<osgShadow::LightSpacePerspectiveShadowMapVB> sm = new osgShadow::LightSpacePerspectiveShadowMapVB;
	ss->setShadowTechnique(sm.get());
	sm->setLight(light);
	sm->setMinLightMargin(minLightMargin);
	sm->setMaxFarPlane(maxFarPlane);
	sm->setTextureSize(osg::Vec2s(texSize, texSize));
	sm->setShadowTextureCoordIndex(shadowTexUnit);
	sm->setShadowTextureUnit(shadowTexUnit);
	sm->setBaseTextureCoordIndex(baseTexUnit);
	sm->setBaseTextureUnit(baseTexUnit);
	ss->setReceivesShadowTraversalMask(ReceivesShadowTraversalMask);
	ss->setCastsShadowTraversalMask(CastsShadowTraversalMask);

	return ss;
}