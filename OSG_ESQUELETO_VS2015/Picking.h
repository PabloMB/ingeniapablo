#pragma once

#include <osgGA/GUIEventHandler>
#include <osgViewer/Viewer>

class PickHandler : public osgGA::GUIEventHandler {
public:

	PickHandler(std::string& p) : path(p) {}
	~PickHandler() {}

	bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);
	virtual void pick(osgViewer::View* view, const osgGA::GUIEventAdapter& ea);

protected:
	std::string& path;
};