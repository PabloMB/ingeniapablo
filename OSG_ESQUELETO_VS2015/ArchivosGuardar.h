#pragma once
#include <string>
#include "inventario.h"
#include <list>

struct ArchivosGuardar {
	double posX;
	double posY;
	double posZ;
	double dir;
	//objetos
	std::list<Objeto> listaYo;
	//camara
	int scroll_times;
	double anguloCamaraVertical0;
	double anguloCamaraHorizontal0;
	double anguloCamaraVertical_1aPersona;
	double anguloCamaraHorizontal_1aPersona;
	//juego
	std::string FASE_JUEGO;
	std::string FASE_JUEGOprev;
	std::string NOMBRE;
	double tiempoCompletado;
	//sol
	double thetaSol;
	double phiSol;
};

int Guardar(ArchivosGuardar datos,std::string nombre);