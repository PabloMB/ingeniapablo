clear
clc
close all

x=0:255;
y=x;
z=x;
value=100;
for i=x+1
    z(1,i) = 255 - abs(value - y(1,i));
    z(1,i) = z(1,i)/255;
end
plot(x,z);
grid on
xlim([0 255])