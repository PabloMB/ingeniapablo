#include "humano.h"
#include "constantesFisicas.h"
#include "constantesHumano.h"
#include "constantesHeightmap.h"

Humano::Humano(double a, double b, double c, double teta) : Punto(a, b, c) {
	setDireccion(teta);
	avance = retroceso = giro_izq = giro_der = salto = false;
	velz = 0;
	inventarioCambiado = false;
}

/*void humano::girar(double teta)
{
	direccion += teta;
}

void humano::avanzar(double d)
{
	setX(getX() + d*cos(direccion));
	setY(getY() + d*sin(direccion));
}*/

void Humano::startSalto(bool a) {
	if (!getSalto() && a) {
		setVelZ(6);
		setZ(getZ()+0.01);
	}
	setSalto(a);
	if (avance)
		setEstadoSalto(1);
	else if (correr)
		setEstadoSalto(2);
	else if (retroceso)
		setEstadoSalto(-1);
	else
		setEstadoSalto(0);
}

void Humano::setSalto(bool a) {
	salto = a;
}

void Humano::setVolando(bool a) {
	volando = a;
	if (avance)
		setEstadoVolando(1);
	else if (correr)
		setEstadoVolando(2);
	else if (retroceso)
		setEstadoVolando(-1);
	else
		setEstadoVolando(0);
}

void Humano::actua(double var_t, osg::Image* image, unsigned int colision[8])
{
	double vx, vy, vz;
	double x1, y1, z1, z1s;
	double x2, y2, z2, z2s;
	x1 = getX();
	y1 = getY();
	z1 = getZ();
	vz = getVelZ();
	double r=0;
	bool algoDelante = (colision[0] == 1)|| (colision[1] == 1) || (colision[7] == 1);
	bool algoDetras = (colision[4] == 1);
	if (!salto && !volando) {
		if (avance) {
			PRINT_MOVIMIENTO("camino\n");
			if (!algoDelante)
				r = 1;
		}
		else if (correr) {
			PRINT_MOVIMIENTO("corro\n");
			if (!algoDelante) {
				r = 3;
			}
		}
		else if (retroceso) {
			PRINT_MOVIMIENTO("retrocedo\n");
			if (!algoDetras)
				r = -1;
		}
		if (giro_izq)
			direccion += CTE_GIRO;
		else if (giro_der)
			direccion -= CTE_GIRO;
	}
	else {
		int e1 = estado_salto;
		int e2 = estado_volando;
		if (e1==1 || e2==1) {
			PRINT_MOVIMIENTO("salto o vuelo caminando\n");
			if (!algoDelante)
				r = 1;
		}
		else if (e1==2 || e2 == 2) {
			PRINT_MOVIMIENTO("salto o vuelo corriendo\n");
			if (!algoDelante) {
				r = 3;
			}
		}
		else if (e1==-1 || e2 == -1) {
			PRINT_MOVIMIENTO("salto o vuelo retrocediendo\n");
			r = -1;
		}
	}
	//pequeño arreglo para que pueda girar estando cerca del suelo
	if (volando && !salto) {
		z1s = obtenerAltura(x1, y1, image, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
		if ((z1>z1s) && (z1-z1s)<5.0) {
			if (giro_izq)
				direccion += CTE_GIRO;
			else if (giro_der)
				direccion -= CTE_GIRO;
		}
	}
	//calculamos coordenadas y velocidades
	vx = r*VEL_ANDAR*cos(direccion);
	vy = r*VEL_ANDAR*sin(direccion);
	x2 = x1 + vx*var_t;
	y2 = y1 + vy*var_t;
	z1s = obtenerAltura(x1, y1, image, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
	z2s = obtenerAltura(x2, y2, image, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
	//calculamos pendiente y factor de reduccion de velocidad
	double d = sqrt( pow(x2-x1,2) + pow(y2-y1,2) );
	double pend;
	if(d!=0)
		pend = (z2s-z1s)/d;
	else
		pend = 0;
	double red;
	if (pend >= PENDIENTE_MAX)
		red = 0;
	else if (pend >= 0)
		red = exp(-pend);
	else
		red = 2 - exp(pend);
	/*if(d!=0) pend = (z2s-z1s)/d;
	else pend = 0;
	double red;
	if(pend<=1) red = 1 - pend;
	else red = 0;*/
	//aplicamos factor de reduccion de velocidad
	r *= red;
	//volvemos a calcular coordenadas y velocidades
	vx = r*VEL_ANDAR*cos(direccion);
	vy = r*VEL_ANDAR*sin(direccion);
	x2 = x1 + vx*var_t;
	y2 = y1 + vy*var_t;
	z1s = obtenerAltura(x1, y1, image, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
	z2s = obtenerAltura(x2, y2, image, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
	if (z2s > z1) {
		z2 = z2s;
		setSalto(false);
	}
	else {
		setVolando(true);
		vz = vz + G*var_t;
		z2 = z1 + vz*var_t + 0.5*G*var_t*var_t;
		if (z2 < z2s){
			z2 = z2s;
			vz = 0;
			setVolando(false);
			setSalto(false);
		}
	}
	setX(x2);
	setY(y2);
	setZ(z2);
	setVelZ(vz);
}

void Humano::setDireccion(double teta)
{
	direccion = teta;
}

double Humano::getDireccion()
{
	return direccion;
}

bool Humano::enMovimiento()
{
	if (getAvance() || getCorrer() || getRetroceso())
		return true;
	return false;
}

double Humano::distanciaA(Punto p)
{
	double hx = getX();
	double hy = getY();
	double hz = getZ();
	double px = p.getX();
	double py = p.getY();
	double pz = p.getZ();

	double d = sqrt(pow(hx - px, 2) + pow(hy - py, 2) + pow(hz - pz, 2));

	return d;
}

bool Humano::estaCercaDe(Punto p, double Dmax=DISTANCIA_MIN_A_OTROS)
{
	double hx = getX();
	double hy = getY();
	double hz = getZ();
	double px = p.getX();
	double py = p.getY();
	double pz = p.getZ();
	
	double d = sqrt( pow(hx-px,2) + pow(hy-py,2) + pow(hz-pz,2) );
	
	if (d < Dmax)
		return true;

	return false;
}

void Humano::setPosition(double a, double b, double c, double d)
{
	Punto::setPosition(a, b, c);
	direccion = d;
}

void Humano::añadirObjeto(Objeto obj)
{
	mochila.añadirObjeto(obj);
	inventarioCambiado = true;
}

int Humano::quitarObjeto(Objeto obj)
{
	int bien = mochila.quitarObjeto(obj);
	if(bien==1)
		inventarioCambiado = true;
	return bien;
}

int Humano::eliminarObjeto(Objeto obj)
{
	int bien = mochila.eliminarObjeto(obj);
	if (bien == 1)
		inventarioCambiado = true;
	return bien;
}

int Humano::poseeObjeto(Objeto obj)
{
	return mochila.poseeObjeto(obj);
}

Objeto* Humano::encuentraObjeto(Objeto obj)
{
	return mochila.encuentraObjeto(obj);
}

void Humano::imprimirInventario()
{
	mochila.imprimirLista();
}

Inventario* Humano::getInventario()
{
	return &mochila;
}

void Humano::setInventario(Inventario *inv)
{
	mochila = *inv;
}

void Humano::vaciarInventario()
{
	mochila.vaciarLista();
	inventarioCambiado = true;
}

