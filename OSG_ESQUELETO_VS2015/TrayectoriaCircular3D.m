centroX = 0;
centroY = 0;
centroZ = 0;
radio=1000;
varT=0.00628;
phi=0;
theta=75/180*pi;
VEL_PHI=1;
VEL_THETA=0;%poner una de las velocidades a cero para que su �ngulo no var�e
x=zeros(1,1000);
y=zeros(1,1000);
z=zeros(1,1000);
o=ones(1,1000);
for i=1:1000
    phi=phi+VEL_PHI*varT;
    theta=theta+VEL_THETA*varT;
    x(1,i)=centroX+radio*cos(phi)*cos(theta);
    y(1,i)=centroX+radio*cos(phi)*sin(theta);
    z(1,i)=centroX+radio*sin(phi);
end
plot3(x,y,z,'r','Linewidth',3)
hold on
grid on
plot3(x,y,-1000*o,'g')
plot3(x,1000*o,z,'b')
plot3(1000*o,y,z,'c')
hold off
xlim([-radio radio])
ylim([-radio radio])
zlim([-radio radio])
title('Circular trayectory in 3D')
xlabel('x-axis')
ylabel('y-axis')
zlabel('z-axis')
legend('trayectory','projection on xy','projection on xz','projection on yz')
%mover la leyenda para que quede todo proporcional