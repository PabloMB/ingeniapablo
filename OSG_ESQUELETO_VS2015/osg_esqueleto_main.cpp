﻿#pragma once

#define WIN32_LEAN_AND_MEAN    // stops windows.h including winsock.h
#include <winsock2.h>

//#define MOSTRAR_TECLA 1
//#define MOSTRAR_ARBOLES 1
//#define MOSTRAR_HIERBAS 1
//#define MOSTRAR_CONTROL 1

//#include <iostream>

#include "stdafx.h"
#include "FuncionesAyuda.h"

#include <string>
#include <math.h>
#include <fstream>
#include <vector>
#include <array>
#include <iterator>
#include <algorithm>

#include <osg/AutoTransform>
#include <osg/Billboard>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Group>
#include <osg/Light>
#include <osg/LightSource>
#include <osg/LineWidth>
#include <osg/Material>
#include <osg/MatrixTransform>
#include <osg/Node>
#include <osg/Notify>
#include <osg/PositionAttitudeTransform>
#include <osg/Projection>
#include <osg/ShapeDrawable>
#include <osg/StateSet>
#include <osg/TexEnv>
#include <osg/TexGen>
#include <osg/TexMat>
#include <osg/Texture2D>
#include <osgText/Text>

#include <osgDB/ReadFile>
#include <osgDB/Registry>
#include <osgDB/WriteFile>

#include <osgGA/TrackballManipulator>
#include <osgGA/StateSetManipulator>

#include <osgShadow/ShadowedScene>
#include <osgShadow/ShadowVolume>
#include <osgShadow/ShadowTexture>
#include <osgShadow/ShadowMap>
#include <osgShadow/SoftShadowMap>
#include <osgShadow/ParallelSplitShadowMap>
#include <osgShadow/LightSpacePerspectiveShadowMap>
#include <osgShadow/StandardShadowMap>
#include <osgShadow/ViewDependentShadowMap>

#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>

#include <osgWidget/WindowManager>
#include <osgWidget/ViewerEventHandlers>
#include <osgWidget/Box>

#include <osg/LineSegment>
#include <osgUtil/IntersectVisitor>

//#include "constantes.h"
#include "constantesHumano.h"
#include "constantesCasa.h"
#include "constantesSol.h"
#include "constantesCamara.h"
#include "constantesWindowManager.h"
#include "constantesMapa.h"
#include "constantesHeightmap.h"
#include "constantesArbolesYHierbas.h"
#include "constantesAvion.h"
#include "constantesPartida.h"
#include "constantesDialogos.h"
#include "constantesRayo.h"
#include "Picking.h"

#include "Punto.h"
#include "Humano.h"
#include "Sol.h"
#include "Toast.h"
#include "Dialogos.h"
#include "Inventario.h"
#include "createMatrixTransform.h"
#include "funcionesDelJuego.h"
#include "lucesYsombras.h"
#include "Mapa.h"

#include "rapidxml-1.13/rapidxml.hpp"

#include "SDL.h"
#include "SDL_mixer.h"
#include "AudioGeneral.h"
#include "AudioItemMusica.h"
#include "AudioItemChunk.h"

#include "snapImage.h"
#include "nameInsertedHandler.h"
#include "WidgetMenu.h"
#include "WidgetMap.h"

#include "Guardar.h"
#include "Cargar.h"

#include "TimeCounter.h"

TimeCounter timer;

AudioGeneral AG;
AudioItemChunk chunk_playit;
AudioItemChunk chunk_crows;
AudioItemChunk chunk_countryside;
AudioItemChunk chunk_gun;
AudioItemChunk chunk_shotgun;

osgViewer::Viewer viewer;

enum ETAPA { MENU_INICIO, INSERTAR_NOMBRE, INSERTAR_CARGAR_NOMBRE, JUGAR, GUARDAR, CARGAR, REINICIAR_PARTIDA, ESTADISTICAS, SALIR, MAPA };
ETAPA ETAPA_GENERAL = MENU_INICIO;
std::string FASE_JUEGO = "0";
std::string FASE_JUEGOprev = "0";
std::string NOMBRE="Invitado";
bool ventanaAñadida = false;
bool ayudaMostrada = false;

GuardaDialogos dialogosMax("Max");
GuardaDialogos dialogosJack("Jack");
GuardaDialogos dialogosBarman("Barman");
GuardaDialogos dialogosJefe("Jefe");
GuardaDialogos dialogosGuardia("Guardia");

Dialogo *diaglogoMostrado;
std::string dialogoPersonaje;
std::string dialogoFase;
std::string dialogoDa;
int dialogoConRespuesta;
std::string dialogoNecesario1;
std::string dialogoNecesario2;
std::string dialogoNecesario3;
std::string dialogoSino1;
std::string dialogoSino2;
std::string dialogoSino3;
std::string respuesta1fase = FASE_JUEGO;
std::string respuesta2fase = FASE_JUEGO;
std::string respuesta3fase = FASE_JUEGO;

osg::ref_ptr<osgWidget::Window> boxMap;
osg::ref_ptr<osgWidget::Widget> widgetMap;
Mapa mapa1;

std::string str_duracionDia = "duracion del dia: ";
std::string str_coordMouse = "";
std::string str_tiempoRestante = "te quedan: ";

double tiempoCompletado = 0; //en segundos
double tiempoPerdido = 0; //en segundos

int screenLength = 0;
int screenHeight = 0;
double margin = 0.1; //en tanto por 1
int windowLength = 800;
int windowHeight = 600;

std::list<int> teclasPulsadas;
struct control_teclado
{
	double x;
	double y;
} click;
bool clickIzquierdo = false;

double anguloCamaraVertical0 = 0;
double anguloCamaraHorizontal0 = 0;
double anguloCamaraVertical = 0;
double anguloCamaraHorizontal = 0;
double anguloCamaraVertical_1aPersona = 0;
double anguloCamaraHorizontal_1aPersona = 0;

Toast toast_global;
Solazo Sol(CENTRO_X, CENTRO_Y, CENTRO_Z, RADIO, ANG_PHI0*PI/180.0, ANG_THETA0*PI/180.0);
double Tdia = 0;
//Punto Sol(-70, -50, 20);
//Humano Yo(YO_X0, YO_Y0, YO_Z0, YO_DIRECCION0*PI/180.0);
Humano Yo(YO_X1, YO_Y1, YO_Z1, YO_DIRECCION1*PI/180.0);
bool yoTieneMapa = false;
Humano Max(MAX_X0, MAX_Y0, MAX_Z0, MAX_DIRECCION0);
Humano Jack(JACK_X0, JACK_Y0, JACK_Z0, JACK_DIRECCION0);
Humano Barman(BARMAN_X0, BARMAN_Y0, BARMAN_Z0, BARMAN_DIRECCION0);
Humano Jefe(JEFE_X0, JEFE_Y0, JEFE_Z0, JEFE_DIRECCION0);
Humano Guardia(GUARDIA_X0, GUARDIA_Y0, GUARDIA_Z0, GUARDIA_DIRECCION0);
Punto Avion(AVION_X0, AVION_Y0, AVION_Z0);

Punto CasaMax(CASAMAX_X0, CASAMAX_Y0, CASAMAX_Z0);
Punto Posada(POSADA_X0, POSADA_Y0, POSADA_Z0);
Punto CasaPueblo1(CASAPUEBLO1_X0, CASAPUEBLO1_Y0, CASAPUEBLO1_Z0);
Punto CasaPueblo2(CASAPUEBLO2_X0, CASAPUEBLO2_Y0, CASAPUEBLO2_Z0);
Punto CasaPueblo3(CASAPUEBLO3_X0, CASAPUEBLO3_Y0, CASAPUEBLO3_Z0);
Punto CasaPueblo4(CASAPUEBLO4_X0, CASAPUEBLO4_Y0, CASAPUEBLO4_Z0);
Punto CasaPuebloS(CASAPUEBLOS_X0, CASAPUEBLOS_Y0, CASAPUEBLOS_Z0);

Punto LuzCasaMax(POSADA_X0, POSADA_Y0, POSADA_Z0);
Punto LuzPosada(POSADA_X0, POSADA_Y0, POSADA_Z0+3);
Punto LuzCasaPueblo1(CASAPUEBLO1_X0, CASAPUEBLO1_Y0, CASAPUEBLO1_Z0+3);
Punto LuzCasaPueblo2(CASAPUEBLO2_X0, CASAPUEBLO2_Y0, CASAPUEBLO2_Z0+3);
Punto LuzCasaPueblo3(CASAPUEBLO3_X0, CASAPUEBLO3_Y0, CASAPUEBLO3_Z0+3);
Punto LuzCasaPueblo4(CASAPUEBLO4_X0, CASAPUEBLO4_Y0, CASAPUEBLO4_Z0+3);

Punto Camara(0, 0, 0);
int scroll_times = SCROLL_TIMES0;
int zoom = 0;
bool cam_detras = true;
bool cam_detras_en_cabeza = false;

void calcularPosicionCamara() {
	double anguloHorizontal = 0;
	double anguloVertical = 0;
	if (scroll_times == 0) { //1a persona
		anguloHorizontal = Yo.getDireccion();
		anguloVertical = anguloCamaraVertical_1aPersona;
	}
	else {
		bool mov = Yo.enMovimiento();
		if (mov && anguloCamaraHorizontal != 0)
			anguloCamaraHorizontal *= 0.95;
		/*if (mov && anguloCamaraVertical != 0)
		anguloCamaraVertical *= 0.95;*/
		anguloHorizontal = Yo.getDireccion() + anguloCamaraHorizontal;
		anguloVertical = 3 * PI / 180 * scroll_times + anguloCamaraVertical;
	}
	//double cam_dist = CAM_DIST - CAM_DIST_VAR*scroll_times;
	double cam_dist = pow(1.7, scroll_times) - 1.2;
	if (cam_dist<0) cam_detras = false;
	else cam_detras = true;
	if (cam_dist<0.5 && cam_dist >= 0) cam_detras_en_cabeza = true;
	else cam_detras_en_cabeza = false;
	Camara.setX(Yo.getX() - cam_dist*cos(anguloVertical)*cos(anguloHorizontal));
	Camara.setY(Yo.getY() - cam_dist*cos(anguloVertical)*sin(anguloHorizontal));
	Camara.setZ(Yo.getZ() + cam_dist*sin(anguloVertical));
}
void colocarCamara() {
	if (cam_detras && cam_detras_en_cabeza) {//hay que poner el vector con signo contrario
											 //coger el vector Yo-Camara, normalizarlo y ponerlo delante de Yo
		double x1 = Yo.getX(); //1 para Yo y 2 para Camara
		double y1 = Yo.getY();
		double z1 = Yo.getZ();
		double x2 = Camara.getX();
		double y2 = Camara.getY();
		double z2 = Camara.getZ();
		double dx = x2 - x1;
		double dy = y2 - y1;
		double dz = z2 - z1;
		double d = sqrt(dx*dx + dy*dy + dz*dz);
		viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(Yo.getX() - dx / d*0.2, Yo.getY() - dy / d*0.2, Yo.getZ() - dz / d*0.2 + 2), //desde dónde miramos
			osg::Vec3d(osg::Vec3d(Yo.getX() - dx / d, Yo.getY() - dy / d, Yo.getZ() - dz / d + 2)),										 //hacia dónde miramos
			osg::Vec3d(0, 0, 1));																									 //vector vertical
	}
	else if (cam_detras)
		viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(Camara.getX(), Camara.getY(), Camara.getZ() + 2), //desde dónde miramos
			osg::Vec3d(Yo.getX(), Yo.getY(), Yo.getZ() + 2),			    //hacia dónde miramos
			osg::Vec3d(0, 0, 1));									    //vector vertical
	else {
		//coger el vector Yo-Camara, normalizarlo y ponerlo delante de Yo
		double x1 = Yo.getX(); //1 para Yo y 2 para Camara
		double y1 = Yo.getY();
		double z1 = Yo.getZ();
		double x2 = Camara.getX();
		double y2 = Camara.getY();
		double z2 = Camara.getZ();
		double dx = x2 - x1;
		double dy = y2 - y1;
		double dz = z2 - z1;
		double d = sqrt(dx*dx + dy*dy + dz*dz);
		viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(Yo.getX() + dx / d*0.2, Yo.getY() + dy / d*0.2, Yo.getZ() + dz / d*0.2 + 2), //desde dónde miramos
			osg::Vec3d(osg::Vec3d(Yo.getX() + dx / d, Yo.getY() + dy / d, Yo.getZ() + dz / d + 2)),								 //hacia dónde miramos
			osg::Vec3d(0, 0, 1));																							 //vector vertical
	}
}

/*#define WAV_PATH "Sonidos/Crows.wav"
#define MUS_PATH "Sonidos/play_it.wav"
Mix_Chunk *wave = NULL;
Mix_Music *music = NULL;
int audio_suena() {
// Initialize SDL.
if (SDL_Init(SDL_INIT_AUDIO) < 0)
return -1;

//Initialize SDL_mixer
if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
return -1;

// Load our sound effect
wave = Mix_LoadWAV(WAV_PATH);
if (wave == NULL)
return -1;

// Load our music
music = Mix_LoadMUS(MUS_PATH);
if (music == NULL)
return -1;

if (Mix_PlayChannel(-1, wave, 0) == -1)
return -1;

if (Mix_PlayMusic(music, 0) == -1) //último argumento: veces que se repite (-1=infinitas veces)
return -1;

while (Mix_PlayingMusic());

// clean up our resources
Mix_FreeChunk(wave);
Mix_FreeMusic(music);

// quit SDL_mixer
Mix_CloseAudio();

return 0;
}*/


//osg::ref_ptr<osg::Image> captura = new osg::Image;
//void capturarPantalla() {
//	osg::Viewport* viewport = viewer.getCamera()->getViewport();
//	std::string filename = "SnapImage.png";
//	//osg::ref_ptr<osg::Image> captura = new osg::Image;
//	if (viewport && captura.valid())
//	{
//		printf("capturando pantalla\n");
//		printf("tamaño de imagen: %d x %d\n", int(viewport->width()), int(viewport->height()));
//		captura->readPixels(int(viewport->x()), int(viewport->y()), int(viewport->width()), int(viewport->height()), GL_RGBA, GL_UNSIGNED_BYTE);
//		if (osgDB::writeImageFile(*captura, filename))
//		{
//			printf("imagen guardada\n");
//		}
//		else
//		{
//			printf("error al guardar la imagen\n");
//		}
//	}
//	else
//		printf("imposible hacer captura\n");
//}

void GetDesktopResolution(int& horizontal, int& vertical)
{
	RECT desktop;
	// Get a handle to the desktop window
	const HWND hDesktop = GetDesktopWindow();
	// Get the size of screen to the variable desktop
	GetWindowRect(hDesktop, &desktop);
	// The top left corner will have coordinates (0,0)
	// and the bottom right corner will have coordinates
	// (horizontal, vertical)
	horizontal = desktop.right;
	vertical = desktop.bottom;
}

//hay que comprobar que funcione
void GetWindowResolution(unsigned int& width, unsigned int& height)
{

	/*unsigned int width = 0, height = 0;
	Producer::RenderSurface* rendersurface = viewer.getCamera()->getRenderSurface();
	rendersurface->getWindowRectangle(x, y, width, height);
	printf("Window size & pos: %dx%d at (%d,%d)\n", width, height, x, y);*/

	osg::ref_ptr<osg::GraphicsContext::WindowingSystemInterface> wsi = osg::GraphicsContext::getWindowingSystemInterface();
	if (!wsi) {
		printf("Error, no WindowSystemInterface available, cannot create windows.\n");
		return;
	}
	wsi->getScreenResolution(osg::GraphicsContext::ScreenIdentifier(0), width, height);
}

void setWindowTitle(osgViewer::Viewer& viewer, std::string title) {

	osg::ref_ptr< osg::GraphicsContext::Traits > traits =
		new osg::GraphicsContext::Traits(*viewer.getCamera()->getGraphicsContext()->getTraits());

	traits->windowName = title;
	
	osg::ref_ptr< osg::GraphicsContext > gc = osg::GraphicsContext::createGraphicsContext(traits.get());
	osg::ref_ptr< osg::Camera > cam = new osg::Camera(*viewer.getCamera());
	cam->setGraphicsContext(gc);
	viewer.setCamera(cam.get());
}
void getWindowSize(osgViewer::Viewer& viewer, int& length, int& height) {

	osg::ref_ptr< osg::GraphicsContext::Traits > traits =
		new osg::GraphicsContext::Traits(*viewer.getCamera()->getGraphicsContext()->getTraits());

	length = traits->width;
	height = traits->height;
}

class SimulacionKeyboardEvent : public osgGA::GUIEventHandler
{
protected:
public:
	SimulacionKeyboardEvent::SimulacionKeyboardEvent()
	{
	};
	struct control_teclado
	{
		double x;
		double y;

	} tecla_pulsada;

	virtual bool SimulacionKeyboardEvent::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter&)
	{

		int tecla = ea.getKey();
		int evento = ea.getEventType();
		int shift = ea.getModKeyMask();
		int button = ea.getButton();
		int scroll = ea.getScrollingMotion();
		tecla_pulsada.x = ea.getX(); //¡devuelve enteros, no float!
		tecla_pulsada.y = ea.getY(); //¡devuelve enteros, no float!

									 //printf("evento: %d\n", evento);
		switch (evento)
		{
			//std::cout << evento << endl;
			case(osgGA::GUIEventAdapter::KEYDOWN):	//Evento pulsacion tecla
			{
				PRINT_KEY("tecla %c (%d) pulsada\n", tecla, tecla);
				{
					bool teclaYaPulsada = std::find(teclasPulsadas.begin(), teclasPulsadas.end(), tecla) != teclasPulsadas.end();
					if (!teclaYaPulsada)
						teclasPulsadas.push_back(tecla);
				}
				switch (tecla)
				{
					case 'W':
					case 'w':
					{
						static double t1 = 0; //por ser static, esta linea solo se ejecuta una vez
						double t2 = ::GetCurrentTime();
						if ((t2 - t1) < 1000 && Yo.getAvance() == false && Yo.getCorrer() == false) {
							Yo.setCorrer(true);
							Yo.setRetroceso(false);
						}
						else if (Yo.getCorrer() == false) {
							Yo.setAvance(true);
							Yo.setRetroceso(false);
							t1 = t2;
						}
					}
					break;
					case 'S':
					case 's':
						Yo.setAvance(false);
						Yo.setRetroceso(true);
						break;
					case 'D':
					case 'd':
						//Yo.girar(-GIRO);
						Yo.setGiroDer(true);
						Yo.setGiroIzq(false);
						break;
					case 'A':
					case 'a':
						//Yo.girar(GIRO);
						Yo.setGiroDer(false);
						Yo.setGiroIzq(true);
						break;
					case 32: //barra espaciadora
						Yo.startSalto(true);
						break;
					case 65451://+
						if (cam_detras && !cam_detras_en_cabeza) {
							if (scroll_times == 0) {
								anguloCamaraHorizontal_1aPersona = 0;
								anguloCamaraVertical_1aPersona = 0;
							}
							else if (scroll_times > 0) {
								scroll_times--;
								if (scroll_times == 0)
									viewer.requestWarpPointer(windowLength / 2, windowHeight / 2);
							}
						}
						zoom++;
						break;
					case 65453://-
						if (scroll_times<SCROLL_TIMES_MAX)
							scroll_times++;
						zoom--;
						break;
					//case 'p':
						//reservado para capturas de pantalla
						//break;
					case 'l':
						//osgViewer::GraphicsWindow graphicsWindow;  //set mouse position???
						//graphicsWindow.requestWarpPointer(0.5, 0.5);
						viewer.requestWarpPointer(windowLength / 2, windowHeight / 2);
						break;
					case 'n':
						toast_global.addMessage("Hola", 4);
						break;
					default:
						break;
				}
				break;
			}
			case(osgGA::GUIEventAdapter::KEYUP):	// Se deja de pulsar una tecla
			{
				teclasPulsadas.remove(tecla);
				PRINT_KEY("tecla %c levantada\n", tecla);
				switch (tecla)
				{
					case 'W':
					case 'w':
						//Yo.avanzar(AVANCE);
						Yo.setAvance(false);
						Yo.setCorrer(false);
						break;
					case 'S':
					case 's':
						//Yo.avanzar(-AVANCE);
						Yo.setRetroceso(false);
						break;
					case 'D':
					case 'd':
						//Yo.girar(-GIRO);
						Yo.setGiroDer(false);
						break;
					case 'A':
					case 'a':
						//Yo.girar(GIRO);
						Yo.setGiroIzq(false);
						break;
					case 'b':
						chunk_playit.play(1);
						break;
					case 'i':
						Yo.imprimirInventario();
						break;
					case 'm':
					{
						if (yoTieneMapa) {
							static bool mapaAgrandado = false;
							if (mapaAgrandado) {
								widgetMap->setSize(MAP_MIN_SIZE, MAP_MIN_SIZE);
								boxMap->resize(MAP_MIN_SIZE, MAP_MIN_SIZE);
								mapaAgrandado = false;
							}
							else {
								widgetMap->setSize(MAP_MAX_SIZE, MAP_MAX_SIZE);
								boxMap->resize(MAP_MAX_SIZE, MAP_MAX_SIZE);
								mapaAgrandado = true;
							}
						}
						break;
					}
					default:
						break;
						/*case 'c':
						tecla_pulsada.x += 1;
						break;
						case 'v':
						tecla_pulsada.x -= 1;
						break;
						default:
						break;*/
				}
				break;
			}
			case(osgGA::GUIEventAdapter::SCROLL):
			{
				PRINT_KEY("scroll motion: %d\n", scroll);
				switch (scroll)
				{
				case osgGA::GUIEventAdapter::SCROLL_UP: //=3
					if (cam_detras && !cam_detras_en_cabeza) {
						if (scroll_times == 0) {
							anguloCamaraHorizontal_1aPersona = 0;
							anguloCamaraVertical_1aPersona = 0;
						}
						else if (scroll_times > 0) {
							scroll_times--;
							if (scroll_times == 0)
								viewer.requestWarpPointer(windowLength / 2, windowHeight / 2);
						}
					}
					zoom++;
					break;
				case osgGA::GUIEventAdapter::SCROLL_DOWN: //=4
					if (scroll_times<SCROLL_TIMES_MAX)
						scroll_times++;
					zoom--;
					break;
				}
				break;
			}
			case(osgGA::GUIEventAdapter::PUSH):
			{
				PRINT_KEY("push\n");
				switch (button)
				{
				case osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON:
					clickIzquierdo = true;
					click.x = tecla_pulsada.x;
					click.y = tecla_pulsada.y;
					anguloCamaraHorizontal0 = anguloCamaraHorizontal;
					anguloCamaraVertical0 = anguloCamaraVertical;
					break;
				case osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON:

					break;
				}
				break;
			}
			case(osgGA::GUIEventAdapter::RELEASE):
			{
				PRINT_KEY("release\n");
				switch (button)
				{
				case osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON:
					clickIzquierdo = false;
					//anguloCamaraHorizontal += (click.x - tecla_pulsada.x)/800*2;
					//anguloCamaraVertical += (click.y - tecla_pulsada.y)/800*2;
					break;
				case osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON:

					break;
				}
				break;
				break;
			}
		}
		if (clickIzquierdo) {
			anguloCamaraHorizontal = anguloCamaraHorizontal0 + (click.x - tecla_pulsada.x) / 800 * 2;
			anguloCamaraVertical = anguloCamaraVertical0 + (click.y - tecla_pulsada.y) / 800 * 2;
		}

		/*str_coordMouse = "";
		str_coordMouse.append("Mouse: x=");
		str_coordMouse.append(std::to_string(int(tecla_pulsada.x)));
		str_coordMouse.append(", y=");
		str_coordMouse.append(std::to_string(int(tecla_pulsada.y)));
		str_coordMouse.append(" (pantalla:");
		str_coordMouse.append(std::to_string(int(windowLength)));
		str_coordMouse.append("x");
		str_coordMouse.append(std::to_string(int(windowHeight)));
		str_coordMouse.append(")");*/
		if (scroll_times == 0) {
			static unsigned int veces = 0;
			veces++;
			static double anguloCamaraHorizontal_1aPersona_Var = 0;
			static double anguloCamaraHorizontal_1aPersona_Mantener = 0;
			static double anguloCamaraVertical_1aPersona_Mantener = 0;
			double wl = windowLength;
			double wh = windowHeight;
			double tx = tecla_pulsada.x;
			double ty = tecla_pulsada.y;
			anguloCamaraHorizontal_1aPersona = (wl / 2.0 - tx) / 100.0 + anguloCamaraHorizontal_1aPersona_Mantener;
			anguloCamaraVertical_1aPersona = (wh / 2.0 - ty) / 100.0 + anguloCamaraVertical_1aPersona_Mantener;
			if (veces < 200) {
				Yo.setDireccion(Yo.getDireccion() + anguloCamaraHorizontal_1aPersona - anguloCamaraHorizontal_1aPersona_Var);
				anguloCamaraHorizontal_1aPersona_Var = anguloCamaraHorizontal_1aPersona;
			}
			else {
				veces = 0;
				Yo.setDireccion(Yo.getDireccion() + anguloCamaraHorizontal_1aPersona - anguloCamaraHorizontal_1aPersona_Var);
				anguloCamaraHorizontal_1aPersona_Mantener = anguloCamaraHorizontal_1aPersona;
				anguloCamaraVertical_1aPersona_Mantener = anguloCamaraVertical_1aPersona;
				//anguloCamaraHorizontal_1aPersona_Var = 0;
				viewer.requestWarpPointer(wl / 2.0, wh / 2.0);
			}
			//viewer.requestWarpPointer(windowLength / 2, windowHeight / 2);
		}
		{
			std::list<int>::iterator it = teclasPulsadas.begin();
			if(it != teclasPulsadas.end())
				PRINT_KEY("\nteclas guardadas: ");
			for (; it != teclasPulsadas.end(); ++it)
				PRINT_KEY("%d ", *it);
		}

		return 0;
	}
};

osg::ref_ptr<osgWidget::Window> mostrarInventario(Inventario* inv) {
	osg::ref_ptr<osgWidget::Window> boxInventario = new osgWidget::Box("BOXMINVENTARIO", osgWidget::Box::HORIZONTAL, true);
	boxInventario->setAnchorVertical(osgWidget::Window::VA_BOTTOM);
	boxInventario->setAnchorHorizontal(osgWidget::Window::HA_CENTER);
	boxInventario->getBackground()->setColor(COLOR_GRIS_OSCURO);
	std::list<Objeto> lista = inv->getLista();
	std::list<Objeto>::iterator it = lista.begin();
	int i = 0;
	for (it; it != lista.end(); it++) {
		boxInventario->addWidget(new ObjetoWidget(it->getName(), windowHeight*0.1, windowHeight*0.1));
		i++;
		if (i == 9) {
			boxInventario->addWidget(new ObjetoWidget("...", windowHeight*0.1, windowHeight*0.1));
			break;
		}
	}

	return boxInventario;
}


int main(int, char**)
{
	timer.takeTime();
	PRINT_CONTROL("EMPEZANDO\n");

	GetDesktopResolution(screenLength, screenHeight);
	printf("pantalla:\n\tlength: %d\n\theight: %d\n", screenLength, screenHeight);
	windowLength = screenLength*(1 - 2 * margin);
	windowHeight = screenHeight*(1 - 2 * margin);

	if (ETAPA_GENERAL != MAPA) {
		viewer.setUpViewInWindow(screenLength*margin, screenHeight*margin, windowLength, windowHeight, 0); //estaba en: 800x600
		setWindowTitle(viewer, "Juego del equipo ESOCIETY");//para poner un título en la ventana
	}
	else {
		viewer.setUpViewInWindow(screenLength*margin, screenHeight*margin, NUM_PUNTOS, NUM_PUNTOS, 0);
		setWindowTitle(viewer, "Juego del equipo ESOCIETY: modo mapa");//para poner un título en la ventana
	}

	//////////////////////////////////////preparar MENU_INICIO///////////////////////////////////////////////////////////////////////////////////
	osg::ref_ptr<osg::Group> grupoMenu = new osg::Group;

	PRINT_CONTROL("PREPARAR MENU INICIO\n");
	osg::ref_ptr<osgWidget::WindowManager> wmMenu = new osgWidget::WindowManager(
		&viewer,
		double(windowLength),
		double(windowHeight),
		MASK_2D//,
		//osgWidget::WindowManager::WM_PICK_DEBUG //esto sirve para mostrar la información de cada widget al pasar el ratón por encima
	);
	osg::ref_ptr<osgWidget::Box> boxMenu = new osgWidget::Box("BOXMENU", osgWidget::Box::VERTICAL);

	osg::ref_ptr<MenuLabel> label1 = new MenuLabel(MENU_OPCION1);
	osg::ref_ptr<MenuLabel> label2 = new MenuLabel(MENU_OPCION2);
	osg::ref_ptr<MenuLabel> label3 = new MenuLabel(MENU_OPCION3);
	osg::ref_ptr<MenuLabel> label4 = new MenuLabel(MENU_OPCION4);
	osg::ref_ptr<MenuLabel> label5 = new MenuLabel(MENU_OPCION5);

	boxMenu->addWidget(label5);
	boxMenu->addWidget(label4);
	boxMenu->addWidget(label3);
	boxMenu->addWidget(label2);
	boxMenu->addWidget(label1);
	//boxMenu->setAnchorVertical(osgWidget::Window::VA_CENTER);
	boxMenu->setAnchorHorizontal(osgWidget::Window::HA_CENTER);
	boxMenu->getBackground()->setColor(COLOR_ROJO_CLARITO);

	boxMenu->setY(windowHeight*0.1);
	wmMenu->addChild(boxMenu);


	osg::ref_ptr<osgText::Font> fontArial = osgText::readRefFontFile("./Otros/fonts/arial.ttf");
	osg::ref_ptr<osgText::Font> fontDirtydoz = osgText::readRefFontFile("./Otros/fonts/dirtydoz.ttf");
	osg::ref_ptr<osgText::Font> fontFudd = osgText::readRefFontFile("./Otros/fonts/fudd.ttf");
	osg::ref_ptr<osgText::Font> fontTimes = osgText::readRefFontFile("./Otros/fonts/times.ttf");
	osg::ref_ptr<osgText::Font> fontVera = osgText::readRefFontFile("./Otros/fonts/Vera.ttf");
	osg::ref_ptr<osgText::Font> fontVeramono = osgText::readRefFontFile("./Otros/fonts/VeraMono.ttf");
	
	//osg::Camera* camera_menu = new osg::Camera;
	osg::ref_ptr<osg::Camera> camera_menu = wmMenu->createParentOrthoCamera();
	camera_menu->setProjectionMatrix(osg::Matrix::ortho2D(-400, 400, -300, 300));
	camera_menu->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
	camera_menu->setViewMatrix(osg::Matrix::identity());
	camera_menu->setClearMask(GL_DEPTH_BUFFER_BIT);
	camera_menu->setRenderOrder(osg::Camera::POST_RENDER);
	//camera_menu->addChild(wmMenu);
	grupoMenu->addChild(camera_menu);

	osg::ref_ptr<osg::Geode> geode_mover_ventana = new osg::Geode();
	osg::ref_ptr<osgText::Text> texto_mover_ventana = new osgText::Text;
	geode_mover_ventana->addDrawable(texto_mover_ventana);
	camera_menu->addChild(geode_mover_ventana);
	//añadido también al resto de cameras
	texto_mover_ventana->setFont(fontArial);
	texto_mover_ventana->setPosition(osg::Vec3(0, -10, 0));
	texto_mover_ventana->setColor(osg::Vec4(255, 255, 255, 255));
	texto_mover_ventana->setFontResolution(40, 40);
	texto_mover_ventana->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_mover_ventana->setCharacterSize(20);
	texto_mover_ventana->setAlignment(osgText::Text::LEFT_BOTTOM);
	/*cargando*/texto_mover_ventana->setText("cargando");

	//viewer.addEventHandler(new osgWidget::MouseHandler(wmMenu));
	////viewer.addEventHandler(new osgWidget::KeyboardHandler(wm));
	//viewer.addEventHandler(new osgWidget::ResizeHandler(wmMenu, camera_menu));
	////viewer.addEventHandler(new osgWidget::CameraSwitchHandler(wm, camera_texto));
	////viewer.addEventHandler(new osgViewer::StatsHandler());
	////viewer.addEventHandler(new osgViewer::WindowSizeHandler());
	////viewer.addEventHandler(new osgGA::StateSetManipulator(
	////	viewer.getCamera()->getOrCreateStateSet()
	////));

	osg::ref_ptr<osg::Group> grupoLuz1 = new osg::Group;
	osg::ref_ptr<osg::Group> grupoTotal = new osg::Group;
	osg::ref_ptr<osg::Group> grupoEscenario = new osg::Group;
	osg::ref_ptr<osg::Group> grupoPueblo = new osg::Group;

	if(ETAPA_GENERAL!=MAPA)
		viewer.setSceneData(grupoMenu);
	else
		viewer.setSceneData(grupoEscenario);
	boxMenu->resize();
	wmMenu->resizeAllWindows();
	viewer.realize();
	viewer.frame();


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	PRINT_CONTROL("CARGANDO\n");

	srand(time(NULL));

	toast_global.setKeyList(&teclasPulsadas);

	osg::ref_ptr<osg::Image> imagen_heightmap = osgDB::readImageFile(HEIGHTMAP_ARCHIVO);
	CasaMax.setZ(obtenerAltura(CasaMax.getX(), CasaMax.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA) - 3);
	Posada.setZ(obtenerAltura(Posada.getX(), Posada.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA) - 3);
	CasaPueblo1.setZ(obtenerAltura(CasaPueblo1.getX(), CasaPueblo1.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA) - 3);
	CasaPueblo2.setZ(obtenerAltura(CasaPueblo2.getX(), CasaPueblo2.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA) - 3);
	CasaPueblo3.setZ(obtenerAltura(CasaPueblo3.getX(), CasaPueblo3.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA) - 3);
	CasaPueblo4.setZ(obtenerAltura(CasaPueblo4.getX(), CasaPueblo4.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA) - 3);
	CasaPuebloS.setZ(obtenerAltura(CasaPuebloS.getX(), CasaPuebloS.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA) - 3);
	osg::ref_ptr<osg::Image> imagen_heightmap_texturas = osgDB::readImageFile(HEIGHTMAP_ARCHIVO_TEXTURAS);

	/*Yo.añadirObjeto(Objeto("pistola", 2));
	Yo.añadirObjeto(Objeto("bala", 3));
	Yo.añadirObjeto(Objeto("lapiz", 1));
	Yo.imprimirInventario();
	Yo.eliminarObjeto(Objeto("lapiz",0));
	Yo.imprimirInventario();
	cambiarDeInventario(Objeto("pistola", 1), *(Yo.getInventario()), *(Jack.getInventario()));*/

	chunk_playit.setPath("Sonidos/play_it.wav");
	chunk_crows.setPath("Sonidos/Crows.wav");
	chunk_countryside.setPath("Sonidos/Countryside2.wav");
	chunk_gun.setPath("Sonidos/Gun fire.wav");
	chunk_shotgun.setPath("Sonidos/Shotgun mossberg 590.wav");
	//chunk_crows.play(1);
	//pulsar 'm' para escuchar "play it"
	printf("volumen anterior del chunk: %d\n", chunk_countryside.setVolume(128));

	///*cargando*/texto_mover_ventana->setText("10%");
	///*cargando*/viewer.frame();

	//para las capturas de pantalla
	osg::ref_ptr<SnapImage> postDrawCallback = new SnapImage("Capturas/Captura");
	viewer.getCamera()->setPostDrawCallback(postDrawCallback);
	viewer.addEventHandler(new SnapeImageHandler('p', postDrawCallback));

	calcularPosicionCamara();

	PRINT_CONTROL("PONIENDO LUCES\n");
	//----------------------------- Luces ----------------------------------
	std::string s1 = Sol.tellDayDuration();
	str_duracionDia.append(s1);
	//Luz global
	osg::ref_ptr<osg::Geode> geodeSol = new osg::Geode();
	grupoEscenario->addChild(geodeSol);
	geodeSol->addDrawable(new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(Sol.getX(), Sol.getY(), Sol.getZ()+5), 2)));//para ver de dónde viene la luz

	osg::ref_ptr<osg::Geode> geodeLuz1 = new osg::Geode();
	grupoEscenario->addChild(geodeLuz1);
	geodeLuz1->addDrawable(new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(LuzPosada.getX(), LuzPosada.getY(), LuzPosada.getZ()-0.3), 0.1)));//para ver de dónde viene la luz 

	//osg::ref_ptr<osg::Light> lightCasaMax = new osg::Light; //la luz de CasaMax
	//lightCasaMax->setLightNum(1);
	//lightCasaMax->setPosition(osg::Vec4(CasaMax.getX()+LUZCASA_POSRELX, CasaMax.getY()+LUZCASA_POSRELY, CasaMax.getZ()+LUZCASA_POSRELZ, 1.0f));		// Posicion foco
	//lightCasaMax->setAmbient(osg::Vec4(0.1f,0.1f,0.1f,1.0f));							// Color luz ambiente (3 primeros valores son RGB, el último es para cantidad)
	//lightCasaMax->setDiffuse(osg::Vec4(0.1f,0.1f,0.1f,1.0f));							// Color luz difusa
 //   //lightCasaMax->setDirection(osg::Vec3(1.0f,0.0f,-1.0f));							// Direccion
	////lightCasaMax->setSpotCutoff(15);													//ángulo del cono de luz (máximo 90 para cono de 180 grados)
	////lightCasaMax->setConstantAttenuation(0.1);										//atenúa la luz en función
	////lightCasaMax->setLinearAttenuation(0.1);											//atenúa la luz en función de la distancia
	////lightCasaMax->setQuadraticAttenuation(1);										//atenúa la luz en función del cuadrado de la distancia
	//osg::ref_ptr<osg::LightSource> lightSourceCasaMax = new osg::LightSource;
	//lightSourceCasaMax->setLight(lightCasaMax);
	//lightSourceCasaMax->setLocalStateSetModes(osg::StateAttribute::ON);

	/*osg::Light* lightCasaMax = new osg::Light;
	lightCasaMax->setLightNum(1);
	lightCasaMax->setPosition(osg::Vec4(CasaMax.getX(), CasaMax.getY(), CasaMax.getZ(), 1.0f));
	lightCasaMax->setAmbient(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f));
	lightCasaMax->setDiffuse(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f));
	lightCasaMax->setSpotCutoff(20.0f);
	lightCasaMax->setSpotExponent(50.0f);
	lightCasaMax->setDirection(osg::Vec3(1.0f, 1.0f, -1.0f));
	lightCasaMax->setQuadraticAttenuation(1);*/
	/*osg::LightSource* lightSourceCasaMax = new osg::LightSource;
	lightSourceCasaMax->setLight(lightCasaMax);
	lightSourceCasaMax->setLocalStateSetModes(osg::StateAttribute::ON);*/
	//lightSourceCasaMax->setStateSetModes(*rootStateSet, osg::StateAttribute::ON);
	osg::Light* lightCasaMax = crearLuzDeInterior(LuzCasaMax.getPunto());
	osg::LightSource* lightSourceCasaMax = crearLightSource(lightCasaMax);

	osg::Light* lightPosada = crearLuzDeInterior(LuzPosada.getPunto());
	osg::LightSource* lightSourcePosada = crearLightSource(lightPosada);

	osg::Light* lightCasaPueblo1 = crearLuzDeInterior(LuzCasaPueblo1.getPunto());
	osg::LightSource* lightSourceCasaPueblo1 = crearLightSource(lightCasaPueblo1);

	osg::Light* lightCasaPueblo2 = crearLuzDeInterior(LuzCasaPueblo2.getPunto());
	osg::LightSource* lightSourceCasaPueblo2 = crearLightSource(lightCasaPueblo2);

	osg::Light* lightCasaPueblo3 = crearLuzDeInterior(LuzCasaPueblo3.getPunto());
	osg::LightSource* lightSourceCasaPueblo3 = crearLightSource(lightCasaPueblo3);

	osg::Light* lightCasaPueblo4 = crearLuzDeInterior(LuzCasaPueblo4.getPunto());
	osg::LightSource* lightSourceCasaPueblo4 = crearLightSource(lightCasaPueblo4);



	osg::ref_ptr<osg::Light> myLight0 = new osg::Light;
	myLight0->setLightNum(0);
	myLight0->setPosition(osg::Vec4(Sol.getX(), Sol.getY(), Sol.getZ(), 1.0f));		// Posicion foco
	myLight0->setAmbient(osg::Vec4(1.0f,1.0f,1.0f,1.0f));							// Color luz ambiente (3 primeros valores son RGB, el último es para cantidad)
	myLight0->setDiffuse(osg::Vec4(0.5f,0.5f,0.5f,1.0f));							// Color luz difusa
    //myLight0->setDirection(osg::Vec3(1.0f,0.0f,-1.0f));							// Direccion
	//myLight0->setSpotCutoff(15);													//ángulo del cono de luz (máximo 90 para cono de 180 grados)
	//myLight0->setConstantAttenuation(0.1);											//atenúa la luz en función
	//myLight0->setLinearAttenuation(0.1);											//atenúa la luz en función de la distancia
	//myLight0->setQuadraticAttenuation(0.1);										//atenúa la luz en función del cuadrado de la distancia
	osg::ref_ptr<osg::LightSource> lightS0 = new osg::LightSource;
	lightS0->setLight(myLight0);
    lightS0->setLocalStateSetModes(osg::StateAttribute::ON);

	osg::ref_ptr<osg::Group> grupoLuz = new osg::Group;
	{
		osg::StateSet* state = grupoTotal->getOrCreateStateSet();
		state->setMode(GL_LIGHTING, osg::StateAttribute::ON);
		state->setMode(GL_LIGHT0, osg::StateAttribute::ON); // para la primera luz
		state->setMode(GL_LIGHT1, osg::StateAttribute::ON); // para la segunda luz
	}
	grupoLuz->addChild(lightS0);
	grupoLuz->addChild(lightSourceCasaMax);
	grupoLuz->addChild(lightSourcePosada);
	grupoLuz->addChild(lightSourceCasaPueblo1);
	grupoLuz->addChild(lightSourceCasaPueblo2);
	grupoLuz->addChild(lightSourceCasaPueblo3);
	grupoLuz->addChild(lightSourceCasaPueblo4);
	grupoTotal->addChild(grupoLuz);


	PRINT_CONTROL("PONIENDO SOMBRAS\n");
	//------------------------------ Sombras ------------------------------------	
	
	const int ReceivesShadowTraversalMask = 0x1;
	const int CastsShadowTraversalMask = 0x2;
	const int ReceivesAndCastsShadowTraversalMask = 0x3;
	float minLightMargin = 10.f;
	float maxFarPlane = 1000;
	unsigned int texSize = 1024;
	unsigned int baseTexUnit = 0;
	unsigned int shadowTexUnit = 7;
	texSize = 4096;

	/*osg::ref_ptr<osgShadow::ShadowedScene> ssCasaMax = new  osgShadow::ShadowedScene;
	osg::ref_ptr<osgShadow::LightSpacePerspectiveShadowMapVB> sm1 = new osgShadow::LightSpacePerspectiveShadowMapVB;
	ssCasaMax->setShadowTechnique(sm1.get());
	sm1->setLight(lightCasaMax);
	sm1->setMinLightMargin( minLightMargin );
	sm1->setMaxFarPlane( maxFarPlane );
 	sm1->setTextureSize( osg::Vec2s( texSize, texSize ) );		
	sm1->setShadowTextureCoordIndex( shadowTexUnit );
	sm1->setShadowTextureUnit( shadowTexUnit );
	sm1->setBaseTextureCoordIndex( baseTexUnit );
	sm1->setBaseTextureUnit( baseTexUnit );
	ssCasaMax->setReceivesShadowTraversalMask(ReceivesShadowTraversalMask);
    ssCasaMax->setCastsShadowTraversalMask(CastsShadowTraversalMask);*/
	osg::ref_ptr<osgShadow::ShadowedScene> ssCasaMax = crearShadowedScene_paraLuzDeInterior(lightCasaMax);
	grupoTotal->addChild(ssCasaMax);
	ssCasaMax->addChild(grupoLuz1);

	osg::ref_ptr<osgShadow::ShadowedScene> ssPosada = crearShadowedScene_paraLuzDeInterior(lightPosada);
	grupoTotal->addChild(ssPosada);
	ssPosada->addChild(grupoLuz1);

	osg::ref_ptr<osgShadow::ShadowedScene> ssCasaPueblo1 = crearShadowedScene_paraLuzDeInterior(lightCasaPueblo1);
	grupoTotal->addChild(ssCasaPueblo1);
	ssCasaPueblo1->addChild(grupoLuz1);

	osg::ref_ptr<osgShadow::ShadowedScene> ssCasaPueblo2 = crearShadowedScene_paraLuzDeInterior(lightCasaPueblo2);
	grupoTotal->addChild(ssCasaPueblo2);
	ssCasaPueblo2->addChild(grupoLuz1);

	osg::ref_ptr<osgShadow::ShadowedScene> ssCasaPueblo3 = crearShadowedScene_paraLuzDeInterior(lightCasaPueblo3);
	grupoTotal->addChild(ssCasaPueblo3);
	ssCasaPueblo3->addChild(grupoLuz1);

	osg::ref_ptr<osgShadow::ShadowedScene> ssCasaPueblo4 = crearShadowedScene_paraLuzDeInterior(lightCasaPueblo4);
	grupoTotal->addChild(ssCasaPueblo4);
	ssCasaPueblo4->addChild(grupoLuz1);

	shadowTexUnit = 6;
	osg::ref_ptr<osgShadow::ShadowedScene> ss = new  osgShadow::ShadowedScene;
	osg::ref_ptr<osgShadow::LightSpacePerspectiveShadowMapVB> sm = new osgShadow::LightSpacePerspectiveShadowMapVB;
	ss->setShadowTechnique(sm.get());
	sm->setLight(myLight0);
	sm->setMinLightMargin(minLightMargin);
	sm->setMaxFarPlane(maxFarPlane);
	sm->setTextureSize(osg::Vec2s(texSize, texSize));
	sm->setShadowTextureCoordIndex(shadowTexUnit);
	sm->setShadowTextureUnit(shadowTexUnit);
	sm->setBaseTextureCoordIndex(baseTexUnit);
	sm->setBaseTextureUnit(baseTexUnit);
	ss->setReceivesShadowTraversalMask(ReceivesShadowTraversalMask);
	ss->setCastsShadowTraversalMask(CastsShadowTraversalMask);
	grupoTotal->addChild(ss);
	ss->addChild(grupoEscenario);

	//viewer.setSceneData(grupoTotal);
	
	//std::cout << "hola!!!";

	///*cargando*/texto_mover_ventana->setText("20%");
	///*cargando*/viewer.frame();

	PRINT_CONTROL("PONIENDO TEXTOS\n");
	//--------------------------- Camara_texto ------------------------------- 
	osg::ref_ptr<osg::Camera> camera_texto = new osg::Camera;
	
	/*osg::ref_ptr<osg::graphicscontext::traits> traits = new osg::graphicscontext::traits;
	traits->x = screenlength*margin;
	traits->y = screenheight*margin;
	traits->width = windowlength;
	traits->height = windowheight;
	traits->windowdecoration = true;
	traits->doublebuffer = true;
	traits->sharedcontext = 0;
	osg::ref_ptr<osg::graphicscontext> gc = osg::graphicscontext::creategraphicscontext(traits.get());
	camera_texto->setgraphicscontext(gc.get());*/

	
	camera_texto->setProjectionMatrix(osg::Matrix::ortho2D(-400,400,-300,300));	
	camera_texto->setReferenceFrame(osg::Transform::ABSOLUTE_RF);			
	camera_texto->setViewMatrix(osg::Matrix::identity());
	camera_texto->setClearMask(GL_DEPTH_BUFFER_BIT);						
	camera_texto->setRenderOrder(osg::Camera::POST_RENDER);	
	camera_texto->setViewport(new osg::Viewport(0, 0, windowLength, windowHeight));
	grupoTotal->addChild(camera_texto);

	osg::ref_ptr<osgWidget::WindowManager> wm = new osgWidget::WindowManager(
		&viewer,
		double(windowLength),
		double(windowHeight),
		MASK_2D//,
		//osgWidget::WindowManager::WM_PICK_DEBUG //esto sirve para mostrar la información de cada widget al pasar el ratón por encima
	);
	
	///*cargando*/texto_mover_ventana->setText("30%");
	///*cargando*/viewer.frame();

	PRINT_CONTROL("BOX MENU\n");
	//osgWidget::Window* box = viewer.getW
	//box->addCallback?
	osg::ref_ptr<osgWidget::Window> menu = new osgWidget::Box("box", osgWidget::Box::VERTICAL);
	menu->setAnchorVertical(osgWidget::Window::VA_TOP);
	menu->setAnchorHorizontal(osgWidget::Window::HA_RIGHT);
	osg::ref_ptr<ColorLabelMenu> labelMenu = new ColorLabelMenu("Menu");
	//ColorLabelMenu* label2 = new ColorLabelMenu("Menu 2");
	menu->addWidget(labelMenu);
	menu->update();
	//menu->addWidget(label2);
	wm->addChild(menu);
	camera_texto->addChild(wm);
	menu->getBackground()->setColor(0.6f, 0.6f, 0.6f, 1.0f);
	menu->resize();
	menu->resizePercent(10.0);
	//osgWidget::createExample(viewer, wm, grupoEscenario);
	
	PRINT_CONTROL("BOX SONIDO\n");
	osg::ref_ptr<osgWidget::Window> boxSonido = new osgWidget::Box("BOXSONIDO", osgWidget::Box::VERTICAL);
	boxSonido->setAnchorVertical(osgWidget::Window::VA_TOP);
	boxSonido->setAnchorHorizontal(osgWidget::Window::HA_LEFT);
	osg::ref_ptr<SonidoLabel> labelSonido = new SonidoLabel("Sonido");
	//ColorLabelMenu* label2 = new ColorLabelMenu("Menu 2");
	boxSonido->addWidget(labelSonido);
	wm->addChild(boxSonido);

	PRINT_CONTROL("BOX AYUDA\n");
	osg::ref_ptr<osgWidget::Box> boxAyuda = new osgWidget::Box("BOXAYUDA", osgWidget::Box::VERTICAL);
	osg::ref_ptr<osgWidget::Label> labelAyuda = new osgWidget::Label("AYUDA");
	labelAyuda->setFontSize(20);
	labelAyuda->setFont(fontTimes->getFileName());
	boxAyuda->setAnchorVertical(osgWidget::Window::VA_CENTER);
	boxAyuda->setAnchorHorizontal(osgWidget::Window::HA_CENTER);
	{
		std::string texto = "Intrucciones:\n";
		texto += "-'wasd' para moverse\n";
		texto += "-barra espaciadora para saltar\n";
		texto += "-click y mover raton para girar la camara\n";
		texto += "-rueda del raton para aumentar y reducir el zoom\n";
		texto += "-click en un personaje para saber quien es\n";
		texto += "-acuérdate de guardar para conservar tu progreso\n";
		texto += "-'m' para agrandar o reducir el mapa\n";
		texto += "-'p' para hacer captura de pantalla\n";
		/*texto += "-\n";
		texto += "-\n";*/
		labelAyuda->setLabel(texto);
	}
	labelAyuda->setColor(osg::Vec4(0.1f, 0.1f, 0.1f, 1.0f));
	boxAyuda->addWidget(labelAyuda);
	//wm->addChild(boxAyuda);

	///*cargando*/texto_mover_ventana->setText("40%");
	///*cargando*/viewer.frame();

	//ventana entera
	//osg::ref_ptr<osgWidget::Box> boxVentana = new osgWidget::Box("BOXVENTANA", osgWidget::Box::VERTICAL);
	//osg::ref_ptr<osgWidget::Widget> ventana = new osgWidget::Widget("VENTANA", windowLength, windowHeight);
	//ventana->setLayer(osgWidget::Widget::LAYER_TOP);
	//ventana->setColor(osg::Vec4(1.0f, 0.0f, 0.0f, 0.0f));
	//osgWidget::Color color = boxVentana->getBackground()->getColor();
	//printf("color inicial de box: (%lf,%lf,%lf,%lf)\n", color.w(), color.x(), color.y(), color.z());
	//boxVentana->getBackground()->setColor(0, 0, 0, 0); //está en (1,1,1,1) por defecto, hay que bajarlo para hacerlo transparente
	//boxVentana->addWidget(ventana);
	////wm->addChild(boxVentana);


	PRINT_CONTROL("BOX VENTANA\n");
	osg::ref_ptr<osgWidget::Box> boxVentana = new osgWidget::Box("BOXVENTANA", osgWidget::Box::VERTICAL);
	osg::ref_ptr<osgWidget::Label> ventana = new osgWidget::Label("VENTANA");
	ventana->setSize(windowLength, windowHeight);
	ventana->setLabel("");
	ventana->setFontSize(25);
	ventana->setFontColor(COLOR_AZUL_CLARITO);
	ventana->setAlignHorizontal(osgWidget::Widget::HorizontalAlignment::HA_LEFT);
	//ventana->setLayer(osgWidget::Widget::LAYER_TOP);
	ventana->setColor(osg::Vec4(1.0f, 0.0f, 0.0f, 0.0f));
	boxVentana->getBackground()->setColor(0, 0, 0, 0); //está en (1,1,1,1) por defecto, hay que bajarlo para hacerlo transparente
	boxVentana->addWidget(ventana);
	//wm->addChild(boxVentana);

	PRINT_CONTROL("BOX CONFIRMAR\n");
	//ventana de confirmar
	osg::ref_ptr<osgWidget::Box> boxConfirmar = new osgWidget::Box("BOXCONFIRMAR", osgWidget::Box::VERTICAL);

	osg::ref_ptr<SentenciaLabel> sentencia = new SentenciaLabel("Tienes un problema", windowLength*0.015, windowHeight*0.025);
	osg::ref_ptr<OpcionLabel> opcionCancelar = new OpcionLabel("Cancelar", windowLength*0.15, windowHeight*0.025);
	osg::ref_ptr<OpcionLabel> opcionAceptar = new OpcionLabel("Aceptar", windowLength*0.15, windowHeight*0.025);

	boxConfirmar->addWidget(opcionCancelar);
	boxConfirmar->addWidget(opcionAceptar);
	boxConfirmar->addWidget(sentencia);
	//boxConfirmar->setAnchorVertical(osgWidget::Window::VA_CENTER);
	boxConfirmar->setAnchorHorizontal(osgWidget::Window::HA_CENTER);
	boxConfirmar->getBackground()->setColor(COLOR_NARANJA);

	//wm->addChild(boxConfirmar);

	PRINT_CONTROL("BOX DIALOGOS\n");
	//para los diálogos
	osg::ref_ptr<osgWidget::Box> boxDialogo = new osgWidget::Box("BOXDIALOGOS", osgWidget::Box::VERTICAL);

	osg::ref_ptr<PreguntaLabel> pregunta = new PreguntaLabel("Frank", "Frase de Frank", windowLength*0.015, windowHeight*0.025);
	osg::ref_ptr<RespuestaLabel> respuesta1 = new RespuestaLabel(1, "Repuesta 1", windowLength*0.15, windowHeight*0.025);
	osg::ref_ptr<RespuestaLabel> respuesta2 = new RespuestaLabel(2, "Repuesta 2", windowLength*0.15, windowHeight*0.025);
	osg::ref_ptr<RespuestaLabel> respuesta3 = new RespuestaLabel(3, "Repuesta 3", windowLength*0.15, windowHeight*0.025);
	
	/*boxDialogo->addWidget(respuesta3);
	boxDialogo->addWidget(respuesta2);
	boxDialogo->addWidget(respuesta1);
	boxDialogo->addWidget(pregunta);*/
	/*boxDialogo->setAnchorVertical(osgWidget::Window::VA_TOP);*/
	boxDialogo->setAnchorHorizontal(osgWidget::Window::HA_LEFT);
	boxDialogo->getBackground()->setColor(COLOR_GRIS_OSCURO);

	//boxDialogo->setX(windowLength*0.75);
	boxDialogo->setY(windowHeight*0.40);
	//wm->addChild(boxDialogo);

	///*cargando*/texto_mover_ventana->setText("50%");
	///*cargando*/viewer.frame();

	////////////////////////////MOSTRAR TIEMPO RESTANTE///////////////////////////////////////////////
	PRINT_CONTROL("BOX TIEMPO\n");
	osg::ref_ptr<osgWidget::Window> boxTiempo = new osgWidget::Box("BOXMTIEMPO", osgWidget::Box::HORIZONTAL, true);
	osg::ref_ptr<NumeroWidget> widgetNumero1 = new NumeroWidget(1, windowHeight*0.1, windowHeight*0.1);
	osg::ref_ptr<NumeroWidget> widgetNumero2 = new NumeroWidget(2, windowHeight*0.1, windowHeight*0.1);
	osg::ref_ptr<NumeroWidget> widgetNumeroP = new NumeroWidget('p', windowHeight*0.1, windowHeight*0.1);
	osg::ref_ptr<NumeroWidget> widgetNumero3 = new NumeroWidget(3, windowHeight*0.1, windowHeight*0.1);
	osg::ref_ptr<NumeroWidget> widgetNumero4 = new NumeroWidget(4, windowHeight*0.1, windowHeight*0.1);

	boxTiempo->addWidget(widgetNumero1);
	boxTiempo->addWidget(widgetNumero2);
	boxTiempo->addWidget(widgetNumeroP);
	boxTiempo->addWidget(widgetNumero3);
	boxTiempo->addWidget(widgetNumero4);
	boxTiempo->setAnchorVertical(osgWidget::Window::VA_TOP);
	boxTiempo->setAnchorHorizontal(osgWidget::Window::HA_LEFT);
	boxTiempo->getBackground()->setColor(COLOR_GRIS_OSCURO);

	//wm->addChild(boxTiempo);

	////////////////////////////MOSTRAR INVENTARIO///////////////////////////////////////////////
	PRINT_CONTROL("BOX INVENTARIO\n"); 
	osg::ref_ptr<osgWidget::Window> boxInventario = new osgWidget::Box("BOXINVENTARIO", osgWidget::Box::HORIZONTAL, true);
	osg::ref_ptr<ObjetoWidget> widgetObjeto1 = new ObjetoWidget("pistola", windowHeight*0.1, windowHeight*0.1);
	osg::ref_ptr<ObjetoWidget> widgetObjeto2 = new ObjetoWidget("lapiz", windowHeight*0.1, windowHeight*0.1);

	boxInventario->setAnchorVertical(osgWidget::Window::VA_BOTTOM);
	boxInventario->setAnchorHorizontal(osgWidget::Window::HA_CENTER);
	boxInventario->getBackground()->setColor(COLOR_GRIS_OSCURO);
	boxInventario->addWidget(widgetObjeto1);
	boxInventario->addWidget(widgetObjeto2);

	boxInventario = mostrarInventario(Yo.getInventario());

	wm->addChild(boxInventario);

	/*osgWidget::Window* boxInventario = new osgWidget::Box("BOXINVENTARIO", osgWidget::Box::HORIZONTAL, true);
	osgWidget::Window* boxObjeto1 = new osgWidget::Box("BOXOBJETO", osgWidget::Box::HORIZONTAL, true);
	osgWidget::Window* boxObjeto2 = new osgWidget::Box("BOXOBJETO", osgWidget::Box::HORIZONTAL, true);
	ObjetoWidget* widgetObjeto1 = new ObjetoWidget("pistola", windowHeight*0.1, windowHeight*0.1);
	ObjetoWidget* widgetObjeto2 = new ObjetoWidget("lapiz", windowHeight*0.1, windowHeight*0.1);

	boxObjeto1->addWidget(widgetObjeto1);
	boxObjeto2->addWidget(widgetObjeto2);
	boxInventario->addChild(boxObjeto1);
	boxInventario->addChild(boxObjeto2);
	boxInventario->setAnchorVertical(osgWidget::Window::VA_BOTTOM);
	boxInventario->setAnchorHorizontal(osgWidget::Window::HA_CENTER);
	boxInventario->getBackground()->setColor(COLOR_GRIS_OSCURO);
	//ColorLabel* c = new ColorLabel("Numero");
	wm->addChild(boxInventario);
	boxObjeto1->resize();
	boxObjeto1->resize();
	boxInventario->resize();*/
	///*cargando*/texto_mover_ventana->setText("60%");
	///*cargando*/viewer.frame();
	////////////////////////////////////DIÁLOGOS/////////////////////////////////////
	PRINT_CONTROL("ENCONTRAR DIALOGOS\n");
	//diálogos de Max
	dialogosMax.encontrarDialogos();
	dialogosJack.encontrarDialogos();
	dialogosBarman.encontrarDialogos();
	dialogosJefe.encontrarDialogos();
	dialogosGuardia.encontrarDialogos();
	//para el MAPA

	/*{
		double t1, t2;
		t1 = ::GetCurrentTime();
		CopyFile(HEIGHTMAP_ARCHIVO, MAPA_ARCHIVO, FALSE);
		t2 = ::GetCurrentTime();
		printf("tiempo en copiar el archivo: %lf ms", t2 - t1);
	}*/
	PRINT_CONTROL("CARGAR MAPA\n");
	/*osg::ref_ptr<osg::Image> imagenMapa = osgDB::readImageFile(MAPA_ARCHIVO_BIS);
	osg::ref_ptr<osg::Image> imagenMapa2 = osgDB::readImageFile(MAPA_ARCHIVO_BIS);
	osg::ref_ptr<osg::Image> imagenCursor = osgDB::readImageFile(MAPA_CURSOR);*/
	mapa1.setImagen(osgDB::readImageFile(MAPA_ARCHIVO_BIS));
	mapa1.setImagenOriginal(osgDB::readImageFile(MAPA_ARCHIVO_BIS));
	mapa1.setImagenCursor(osgDB::readImageFile(MAPA_CURSOR));
	//youAreAt(Yo.getX(), Yo.getY(), imagenMapa, NUM_PUNTOS);
	

	//osg::Image* imagenMapaBYN = osgDB::readImageFile(MAPA_ARCHIVO);
	//osg::Image* imagenMapaFinal = osgDB::readImageFile(MAPA_ARCHIVO);
	//unsigned char* c = imagenMapaBYN->data();
	////printf("imageMapa:\n%s\n", c);
	//{
	//	int i = 0;
	//	while (c[i] != 3) {
	//		if(c[i]==0xFF)
	//			printf("%c", c[i]);
	//		else
	//			printf("%02X", int(c[i]));
	//		i++;
	//	}
	//	printf("\ncaracteres: %d\n", i - 1);
	//}
	////printf("%s\n", c);
	//system("pause");

	///*cargando*/texto_mover_ventana->setText("70%");
	///*cargando*/viewer.frame();
	PRINT_CONTROL("BOX MAPA\n");
	/*osgWidget::Window**/ boxMap = new osgWidget::Box("BOXMAP", osgWidget::Box::HORIZONTAL, true);
	/*osgWidget::Widget**/ widgetMap = new ColorWidget("WIDGETMAP", 256.0f, 256.0f);
	//widgetMap->setImage(imagenMapa);
	widgetMap->setImage(mapa1.getImagen());
	widgetMap->setTexCoord(0.0f, 0.0f, osgWidget::Widget::LOWER_LEFT);
	widgetMap->setTexCoord(1.0f, 0.0f, osgWidget::Widget::LOWER_RIGHT);
	widgetMap->setTexCoord(1.0f, 1.0f, osgWidget::Widget::UPPER_RIGHT);
	widgetMap->setTexCoord(0.0f, 1.0f, osgWidget::Widget::UPPER_LEFT);
	widgetMap->setSize(200,200);
	boxMap->addWidget(widgetMap);
	if (!yoTieneMapa) {
		if (Yo.poseeObjeto(Objeto(std::string("mapa")))) {
			wm->addChild(boxMap);
			yoTieneMapa = true;
		}
	}

	PRINT_CONTROL("TEXTOS\n");
	//----------------------------- Texto ----------------------------
	/*osg::ref_ptr<osgText::Font> font = osgText::readRefFontFile("./Otros/fonts/arial.ttf");*/
	/*osg::Geode* geode_texto_intro = new osg::Geode();
	osgText::Text* texto_intro = new osgText::Text;
	geode_texto_intro->addDrawable(texto_intro);
	cera_texto->addChild(geode_texto_intro);
	texto_intro->setFont(font);
	texto_intro->setPosition(osg::Vec3(0, windowHeight*0.7, 0));
	texto_intro->setColor(osg::Vec4(255, 255, 255, 255));
	texto_intro->setFontResolution(40,40);
	texto_intro->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_intro->setCharacterSize(20);
	texto_intro->setAlignment(osgText::Text::LEFT_BOTTOM);
	texto_intro->setText(str_duracionDia);*/

	//mostrar coordenadas del personaje
	osg::ref_ptr<osg::Geode> geode_texto_intro2 = new osg::Geode();
	osg::ref_ptr<osgText::Text> texto_intro2 = new osgText::Text;
	geode_texto_intro2->addDrawable(texto_intro2);
	camera_texto->addChild(geode_texto_intro2);
	texto_intro2->setFont(fontArial);
	texto_intro2->setPosition(osg::Vec3(0, windowHeight*0.85, 0));
	texto_intro2->setColor(osg::Vec4(255, 255, 255, 255));
	texto_intro2->setFontResolution(40, 40);
	texto_intro2->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_intro2->setCharacterSize(20);
	texto_intro2->setAlignment(osgText::Text::LEFT_TOP);
	//texto_intro2->setText("coordenadas del personaje");

	//mostrar posición del ratón
	osg::ref_ptr<osg::Geode> geode_texto_intro3 = new osg::Geode();
	osg::ref_ptr<osgText::Text> texto_intro3 = new osgText::Text;
	geode_texto_intro3->addDrawable(texto_intro3);
	camera_texto->addChild(geode_texto_intro3);
	texto_intro3->setFont(fontArial);
	texto_intro3->setPosition(osg::Vec3(-400, -250, 0));
	texto_intro3->setColor(osg::Vec4(255, 255, 255, 255));
	texto_intro3->setFontResolution(40, 40);
	texto_intro3->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_intro3->setCharacterSize(20);
	texto_intro3->setAlignment(osgText::Text::LEFT_BOTTOM);
	texto_intro3->setText("");

	//toast
	osg::ref_ptr<osg::Geode> geode_toast = new osg::Geode();
	osg::ref_ptr<osgText::Text> texto_toast = new osgText::Text;
	geode_toast->addDrawable(texto_toast);
	camera_texto->addChild(geode_toast);
	texto_toast->setFont(fontArial);
	texto_toast->setPosition(osg::Vec3(windowLength/2, windowHeight*0.15, 0));
	texto_toast->setColor(osg::Vec4(255, 255, 255, 255));
	texto_toast->setFontResolution(40, 40);
	texto_toast->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_toast->setCharacterSize(20);
	texto_toast->setAlignment(osgText::Text::CENTER_BOTTOM);
	texto_toast->setText("toast");

	//tiempo restante
	/*osg::Geode* geode_tiempo = new osg::Geode();
	osgText::Text* texto_tiempo = new osgText::Text;
	geode_tiempo->addDrawable(texto_tiempo);
	camera_texto->addChild(geode_tiempo);
	texto_tiempo->setFont(font);
	texto_tiempo->setPosition(osg::Vec3(windowLength/2, windowHeight*0.9, 0));
	texto_tiempo->setColor(osg::Vec4(255, 255, 255, 255));
	texto_tiempo->setFontResolution(40, 40);
	texto_tiempo->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_tiempo->setCharacterSize(50);
	texto_tiempo->setAlignment(osgText::Text::CENTER_BOTTOM);
	texto_tiempo->setText(str_tiempoRestante);*/

	/*osg::Geode* geode = new osg::Geode();
	osgText::Text* texto_intro4 = new osgText::Text;
	geode->addDrawable(texto_intro4);
	//botton->addDescription(new osg::ShapeDrawable(new osg::)
	camera_texto->addChild(geode);
	texto_intro4->setFont(font);
	texto_intro4->setPosition(osg::Vec3(0, 0, 0));
	texto_intro4->setColor(osg::Vec4(255, 255, 255, 255));
	texto_intro4->setFontResolution(80, 80);
	texto_intro4->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_intro4->setCharacterSize(20);
	texto_intro4->setAlignment(osgText::Text::LEFT_BOTTOM);
	texto_intro4->setText("posición del ratón");*/

	/*osg::BoundingBox bb;
	for (unsigned int i = 0; i<geode->getNumDrawables(); ++i)
	{
		bb.expandBy(geode->getDrawable(i)->getBound());
	}
	osg::Geometry* geom = new osg::Geometry;

	osg::Vec3Array* vertices = new osg::Vec3Array;
	float depth = bb.zMin() - 0.1;
	vertices->push_back(osg::Vec3(bb.xMin(), bb.yMax(), depth));
	vertices->push_back(osg::Vec3(bb.xMin(), bb.yMin(), depth));
	vertices->push_back(osg::Vec3(bb.xMax(), bb.yMin(), depth));
	vertices->push_back(osg::Vec3(bb.xMax(), bb.yMax(), depth));
	geom->setVertexArray(vertices);*/

	/*osg::Vec3Array* normals = new osg::Vec3Array;
	normals->push_back(osg::Vec3(0.0f, 0.0f, 1.0f));
	geom->setNormalArray(normals);
	geom->setNormalBinding(osg::Geometry::BIND_OVERALL);*/

	/*osg::ref_ptr<osg::Vec4Array> c = new osg::Vec4Array;		//la línea con Vec4Array da error: símbolo externo sin resolver (¿falta algun .lib en el input linker?)
	geom->setColorArray(c.get());
	geom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
	c->push_back(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f));
	c->push_back(osg::Vec4(0.0f, 1.0f, 0.0f, 1.0f));
	c->push_back(osg::Vec4(0.0f, 0.0f, 1.0f, 1.0f));
	c->push_back(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));*/
	/*osg::ref_ptr<osg::Vec4Array> c = new osg::Vec4Array;
	colors->push_back(osg::Vec4(1.0f, 1.0, 0.8f, 0.2f));
	geom->setColorArray(colors);
	geom->setColorBinding(osg::Geometry::BIND_OVERALL);*/

	/*geom->addPrimitiveSet(new osg::DrawArrays(GL_QUADS, 0, 4));*/

	/*osg::Texture2D* tex = new osg::Texture2D;
	osg::Image* image = osgDB::readImageFile("./Heightmap/Texturas/Heightmap_rgb.png");
	tex->setImage(image);
	osg::StateSet* state = geom->getOrCreateStateSet();
	//state->setMode(GL_BLEND, osg::StateAttribute::ON);
	//state->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	state->setTextureAttributeAndModes(0, tex, osg::StateAttribute::ON);
	geode->setStateSet(state);*/
	
	/*geode->addDrawable(geom);*/


	/*osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
	osg::ref_ptr<osg::Vec3Array> v = new osg::Vec3Array;
	geom->setVertexArray(v.get());
	double l = 6;
	v->push_back(osg::Vec3(-l, 0, -l));
	v->push_back(osg::Vec3(l, 0, -l));
	v->push_back(osg::Vec3(l, 0, l));
	v->push_back(osg::Vec3(-l, 0, l));
	osg::ref_ptr<osg::Vec4Array> c = new osg::Vec4Array;*/
	/*geom->setColorArray(c.get());
	geom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
	c->push_back(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f));
	c->push_back(osg::Vec4(0.0f, 1.0f, 0.0f, 1.0f));
	c->push_back(osg::Vec4(0.0f, 0.0f, 1.0f, 1.0f));
	c->push_back(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));*/
	
	/*osg::ref_ptr<osg::Vec3Array> n = new osg::Vec3Array;
	geom->setNormalArray(n.get());
	geom->setNormalBinding(osg::Geometry::BIND_OVERALL);
	n->push_back(osg::Vec3(0.0f, -1.0f, 0.0f));*/
	/*geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::QUADS, 0, 4)); 
	osg::ref_ptr<osg::Geode> newGeode = new osg::Geode;
	newGeode->addDrawable(geom.get());

	osg::Texture2D* tex = new osg::Texture2D;
	osg::Image* image = osgDB::readImageFile("./Heightmap/Texturas/Heightmap_rgb.png");
	tex->setImage(image);
	osg::StateSet* stateOne = new osg::StateSet();
	stateOne->setTextureAttributeAndModes(0, tex, osg::StateAttribute::ON);
	newGeode->setStateSet(stateOne);
	osg::MatrixTransform* mtNew = new osg::MatrixTransform;
	mtNew->addChild(newGeode);
	grupoEscenario->addChild(mtNew);
	osg::Matrixd mtrasP, mrotP, mrotzP, mrotxP, mrotyP, mescP, mtotalP;
	osg::Vec3d pos2(3, -33, 1);
	mtrasP = mtrasP.translate(pos2);
	mrotzP = mrotzP.rotate(0, osg::Vec3d(0, 0, 1));
	mescP = mtrasP.scale(0.1, 0.1, 0.1);
	mtotalP = mescP*mrotP*mtrasP;
	mtNew->setMatrix(mtotalP);*/


	

	//-----------Codigo editado---------//
	//std::cout << "hola";
	//printf("bienvenido\n");

	/*std::string geometria1 = "Tree.obj";
	std::string geometria2 = "Palma 001.obj";
	std::string geometria3 = "FinalBaseMesh.obj";
	osg::Node* nodo = osgDB::readNodeFile("./Texturas/Trees/" + geometria1);//lee en la carpeta tree y le añade la geometria 
	osg::Node* nodo2 = osgDB::readNodeFile("./Texturas/Trees/"+ geometria2);
	osg::Node* nodoH = osgDB::readNodeFile("./Texturas/Humanos/"+ geometria3);
	//grupoEscenario->addChild(nodo); 
	
	osg::MatrixTransform* mt = new osg::MatrixTransform;
	mt->addChild(nodo);
	grupoEscenario->addChild(mt);
	osg::Matrixd mtras, mrot, mrotz, mrotx, mroty, mesc, mtotal, mtras2;
	osg::Vec3d pos(10, 0, 0);
	mtras = mtras.translate(pos);
	mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
	mesc = mtras.scale(0.1, 0.1, 0.1);
	mtotal = mesc*mrot*mtras;
	mt->setMatrix(mtotal);

	osg::MatrixTransform* mt2 = new osg::MatrixTransform;
	mt2->addChild(nodo);
	grupoEscenario->addChild(mt2);
	//osg::Matrixd mtras, mrot, mrotz, mrotx, mroty, mesc, mtotal, mtras2;
	osg::Vec3d pos2(-10, 0, 0);
	mtras = mtras.translate(pos2);
	mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
	mesc = mtras.scale(0.12, 0.12, 0.12);
	mtotal = mesc*mrot*mtras;
	mt2->setMatrix(mtotal);

	osg::MatrixTransform* mt3 = new osg::MatrixTransform;
	mt3->addChild(nodo2);
	grupoEscenario->addChild(mt3);
	//osg::Matrixd mtras, mrot, mrotz, mrotx, mroty, mesc, mtotal, mtras2;
	osg::Vec3d pos3(0, -5, 0);
	mtras = mtras.translate(pos3);
	mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
	mesc = mtras.scale(0.1, 0.1, 0.1);
	mtotal = mesc*mrot*mtras;
	mt3->setMatrix(mtotal);*/

	/*****xml inicio****/

	///*cargando*/texto_mover_ventana->setText("75%");
	///*cargando*/viewer.frame();

	PRINT_CONTROL("BOX MALLA\n");
	//añadimos heightmap
	osg::ref_ptr<osg::Geode> malla = CreaMalla(NUM_PUNTOS, SEPARACION_PUNTOS);	// Crea la malla del suelo
	AddTexture(malla, HEIGHTMAP_ARCHIVO, 0);						// Añade la textura a la malla (heightmap)
	AddTexture(malla, HEIGHTMAP_ARCHIVO_NORMALES, 1);				// Añade la textura a la malla (normales)
	AddTexture(malla, HEIGHTMAP_ARCHIVO_TEXTURAS, 2);				// Añade la textura a la malla (texturas)
	AddTexture(malla, HEIGHTMAP_TEXTURA_CESPED, 3);			// Añade la textura a la malla (textura1)
	AddTexture(malla, HEIGHTMAP_TEXTURA_TIERRA, 4);			// Añade la textura a la malla (textura2)
	AddTexture(malla, HEIGHTMAP_TEXTURA_MONTAÑA, 5);		// Añade la textura a la malla (textura3)
	//textura 6 usada por las sombras
	AddTexture(malla, HEIGHTMAP_TEXTURA_PIEDRA, 7);		// Añade la textura a la malla (textura)
	AddShader(malla, HEIGHTMAP_ARCHIVO_VERT, HEIGHTMAP_ARCHIVO_FRAG);

	osg::ref_ptr<osg::MatrixTransform> mtMalla = new osg::MatrixTransform;
	mtMalla->addChild(malla);
	grupoEscenario->addChild(mtMalla);
	osg::Matrixd mtras5, mrot5, mrotz5, mrotx5, mroty5, mesc5, mtotal5;
	osg::Vec3d pos(0, 0, 0);
	mtras5 = mtras5.translate(pos);
	mrotz5 = mrotz5.rotate(0, osg::Vec3d(0, 0, 1));
	mesc5 = mtras5.scale(1, 1, 1);
	mtotal5 = mesc5*mrot5*mtras5;
	mtMalla->setMatrix(mtotal5);
	
	///*cargando*/texto_mover_ventana->setText("80%");
	///*cargando*/viewer.frame();

	PRINT_CONTROL("HIERBAS\n");
	///////////////////CARGAR HIERBAS EN EL TERRENO/////////////////////////////////////////////////////////////////////////////////////////////////
	{//brackets {} para que no existan variables innecesarias en el resto del código

		rapidxml::xml_document<> doc;
		// Read the xml file into a vector
		osgDB::ifstream theFile(XML_WEEDS);
		std::vector<char> buffer((std::istreambuf_iterator<char>(theFile)), std::istreambuf_iterator<char>());
		buffer.push_back('\0');
		// Parse the buffer using the xml file parsing library into doc 
		doc.parse<0>(&buffer[0]);

		rapidxml::xml_node<> *nodo0;
		nodo0 = doc.first_node();

		//contamos hierbas del archivo weeds.xml
		int i = 0;
		for (rapidxml::xml_node<>* nodo1 = nodo0->first_node("weed"); nodo1; nodo1 = nodo1->next_sibling("weed")) {
			i++;
		}
		printf("numero de hierbas: %d\n", i);

		//contamos geometrías del archivo trees.xml y creamos vectores para guardar sus nombres y sus valores de escalado
		i = 0;
		for (rapidxml::xml_node<>* nodo1 = nodo0->first_node("geometria"); nodo1; nodo1 = nodo1->next_sibling("geometria")) {
			i++;
		}
		printf("numero de geometrias: %d\n", i);
		double *Escala = new double[i];
		std::vector <osg::ref_ptr<osg::Node>> nodoHierbaVector;
		//osg::ref_ptr<osg::Node> nodoG = new osg::Node[i];
		//guardamos los valores de las geometrías en dichos vectores
		i = 0;
		for (rapidxml::xml_node<>* nodo1 = nodo0->first_node("geometria"); nodo1; nodo1 = nodo1->next_sibling("geometria")) {

			//carga nodo con cada geometría una sola vez
			osg::ref_ptr<osg::Node> nodoGG = osgDB::readNodeFile("./Texturas/Weeds/" + std::string(nodo1->value()));
			nodoHierbaVector.push_back(nodoGG);

			//guarda la escala de esa geometría
			std::string strE = nodo1->first_attribute()->value();
			Escala[i] = atof(strE.c_str());

			i++;
		}

		//creamos las hierbas
		i = 0;
		for (rapidxml::xml_node<>* nodo1 = nodo0->first_node("weed"); nodo1; nodo1 = nodo1->next_sibling("weed")) {

			PRINT_HIERBAS("\tCargando hierba %d (%s) ", i + 1, nodo1->name());
			i++;

			//ponemos los árboles en sus posiciones
			std::string str = nodo1->first_node("fichero")->value();
			int g = atoi(str.c_str()); //número de la geometria
			std::string strX = nodo1->first_node("x")->value();
			double x = atof(strX.c_str());//pasa el string a un valor tipo double para usarlo
			std::string strY = nodo1->first_node("y")->value();
			double y = atof(strY.c_str());
			//std::string strZ= nodo1->first_node("z")->value();
			//double z = atof(strZ.c_str());
			//double z = obtenerAltura(x, y, imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
			//osg::ref_ptr<osg::Node> nodoG = osgDB::readNodeFile("./Texturas/Trees/" + strG[g-1]);//añade el nombre a partir del número de la geometría

			//valor aleatorio para que el tamaño de cada árbol sea diferente
			double randomPercentage = 0.8 + rand() % 50 / 100.0; //0.8 más un valor entre 0 y 0.4 (50 posibilidades)
			double randomDirection = rand() % 360 * 2*PI / 360.0; //entre 0 y 2PI radianes
			PRINT_HIERBAS("escala x %lf", randomPercentage);

			//poner árboles solo si la textura del heightmap en ese punto es verde (hierba)
			double xx = (x + NUM_PUNTOS/2) / NUM_PUNTOS;
			double yy = (y + NUM_PUNTOS/2) / NUM_PUNTOS;
			osg::Vec4 color = imagen_heightmap_texturas->getColor(osg::Vec2(xx, yy));

			PRINT_HIERBAS("\tColor: (%lf,%lf,%lf,%lf)", color.r(), color.g(), color.b(), color.a());
			if ( ( (color.b()+color.r())<=0.2 || (color.g()+color.b())<=0.50) && !(x<30.0 && x>-83.0 && y<-56.0 && y>-120.0)) {//que sea verde o rojo y no se encuentre en el pueblo
				double z = obtenerAltura(x, y, imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
				osg::ref_ptr<osg::MatrixTransform> mtHierba = createMatrixTransform(Punto(x, y, z), randomDirection, Escala[g-1]*randomPercentage);
				mtHierba->addChild(nodoHierbaVector.at(g - 1));
				ss->addChild(mtHierba);

				//quién da sombra
				mtHierba->setNodeMask(CastsShadowTraversalMask);
			}

			PRINT_HIERBAS("\n");
		}

	}
	///*cargando*/texto_mover_ventana->setText("85%");
	///*cargando*/viewer.frame();
	PRINT_CONTROL("ARBOLES\n");
	///////////////////CARGAR ÁRBOLES EN EL TERRENO/////////////////////////////////////////////////////////////////////////////////////////////////
	{//brackets {} para que no existan variables innecesarias en el resto del código

			rapidxml::xml_document<> doc;
			// Read the xml file into a vector
			osgDB::ifstream theFile(XML_TREES);
			std::vector<char> buffer((std::istreambuf_iterator<char>(theFile)), std::istreambuf_iterator<char>());
			buffer.push_back('\0');
			// Parse the buffer using the xml file parsing library into doc 
			doc.parse<0>(&buffer[0]);
	
			rapidxml::xml_node<> *nodo0;
			nodo0 = doc.first_node();
		

			//contamos árboles del archivo trees.xml
			int i = 0;
			for (rapidxml::xml_node<>* nodo1 = nodo0->first_node("tree"); nodo1; nodo1 = nodo1->next_sibling("tree")) {
				i++;
			}
			PRINT_ARBOLES("numero de arboles: %d\n", i);

			//contamos geometrías del archivo trees.xml y creamos vectores para guardar sus nombres y sus valores de escalado
			i = 0;
			for (rapidxml::xml_node<>* nodo1 = nodo0->first_node("geometria"); nodo1; nodo1 = nodo1->next_sibling("geometria")) {
				i++;
			}
			PRINT_ARBOLES("numero de geometrias: %d\n", i);
			double *Escala = new double[i];
			std::vector <osg::ref_ptr<osg::Node>> nodoArbolVector;
			//osg::ref_ptr<osg::Node> nodoG = new osg::Node[i];
			//guardamos los valores de las geometrías en dichos vectores
			i = 0;
			for (rapidxml::xml_node<>* nodo1 = nodo0->first_node("geometria"); nodo1; nodo1 = nodo1->next_sibling("geometria")) {

				//carga nodo con cada geometría una sola vez
				osg::ref_ptr<osg::Node> nodoGG = osgDB::readNodeFile("./Texturas/Trees/" + std::string(nodo1->value()));
				nodoArbolVector.push_back(nodoGG);

				//guarda la escala de esa geometría
				std::string strE = nodo1->first_attribute()->value();
				Escala[i] = atof(strE.c_str());

				i++;
				PRINT_ARBOLES("geometría %d\n", i+1);
			}

			//creamos los árboles
			i = 0;
			for (rapidxml::xml_node<>* nodo1 = nodo0->first_node("tree"); nodo1; nodo1 = nodo1->next_sibling("tree")) {
		
				PRINT_ARBOLES("\tCargando arbol %d (%s) ", i + 1, nodo1->name());
				i++;

				//ponemos los árboles en sus posiciones
				std::string str = nodo1->first_node("fichero")->value();
				int g = atoi(str.c_str()); //número de la geometria
				std::string strX= nodo1->first_node("x")->value();
				double x = atof(strX.c_str());//pasa el string a un valor tipo double para usarlo
				std::string strY= nodo1->first_node("y")->value();
				double y = atof(strY.c_str()); 
				//std::string strZ= nodo1->first_node("z")->value();
				//double z = atof(strZ.c_str());
				//double z = obtenerAltura(x, y, imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
				//osg::ref_ptr<osg::Node> nodoG = osgDB::readNodeFile("./Texturas/Trees/" + strG[g-1]);//añade el nombre a partir del número de la geometría
				
				//valor aleatorio para que el tamaño de cada árbol sea diferente
				double randomPercentage = 0.8 + rand()%50/100.0; //0.8 más un valor entre 0 y 0.4 (50 posibilidades)
				double randomDirection = rand() % 360 * PI / 360.0; //entre 0 y 2PI radianes
				PRINT_ARBOLES("escala x %lf", randomPercentage);

				//poner árboles solo si la textura del heightmap en ese punto es verde (hierba)
				double xx = (x + NUM_PUNTOS / 2) / NUM_PUNTOS;
				double yy = (y + NUM_PUNTOS / 2) / NUM_PUNTOS;
				osg::Vec4 color = imagen_heightmap_texturas->getColor(osg::Vec2(xx, yy));
				
				PRINT_ARBOLES("\tColor: (%lf,%lf,%lf,%lf)", color.r(), color.g(), color.b(), color.a());
				if (color.b() <= 0.8) {//que no sea azul
					double z = obtenerAltura(x, y, imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
					osg::ref_ptr<osg::MatrixTransform> mtArbol = createMatrixTransform(Punto(x, y, z), randomDirection, Escala[g - 1] * randomPercentage);
					mtArbol->addChild(nodoArbolVector.at(g - 1));
					grupoEscenario->addChild(mtArbol);

					//quién da sombra
					mtArbol->setNodeMask(CastsShadowTraversalMask);
				}
				

				//double z = obtenerAltura(x, y, imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
				//osg::ref_ptr<osg::MatrixTransform> mt = createMatrixTransform(Punto(x,y,z), 0, Escala[g-1]*randomPercentage);
				//mt->addChild(nodoGGvector.at(g-1));
				//grupoEscenario->addChild(mt);

				////quién da sombra
				//mt->setNodeMask(CastsShadowTraversalMask);

				PRINT_ARBOLES("\n");
			}

	}

	///*cargando*/texto_mover_ventana->setText("90%");
	///*cargando*/viewer.frame();

	PRINT_CONTROL("AVION\n");
	/*****xml fin******/
	{
		osg::ref_ptr<osg::Node> nodoAvion = osgDB::readNodeFile("./Texturas/Avion/avion.obj");
		osg::ref_ptr<osg::MatrixTransform> mtAvion = new osg::MatrixTransform;
		mtAvion->addChild(nodoAvion);
		grupoEscenario->addChild(mtAvion);
		osg::Matrixd mtrasA, mrotA, mrotzA, mrotxA, mrotyA, mescA, mtotalA;
		double z = obtenerAltura(AVION_X0, AVION_Y0, imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
		osg::Vec3d posA(AVION_X0, AVION_Y0, z-AVION_BAJARZ);
		mtrasA = mtrasA.translate(posA);
		mrotxA = mrotxA.rotate(AVION_ANGX0*PI/180.0, osg::Vec3d(1, 0, 0));
		mrotyA = mrotyA.rotate(AVION_ANGY0*PI/180.0, osg::Vec3d(0, 1, 0));
		mrotzA = mrotzA.rotate(AVION_ANGZ0*PI/180.0, osg::Vec3d(0, 0, 1));
		mrotA = mrotxA*mrotyA*mrotzA;
		mescA = mescA.scale(1.3, 1.3, 1.3);
		mtotalA = mescA*mrotA*mtrasA;
		mtAvion->setMatrix(mtotalA);
	}

	PRINT_CONTROL("YO\n");
	osg::ref_ptr<osg::Node> nodoH = osgDB::readNodeFile("./Texturas/Humanos/forlan/forlan.obj");
	//osg::Node* nodoH = osgDB::readNodeFile("./Texturas/Humanos/biped1.osg");
	osg::ref_ptr<osg::MatrixTransform> mtH = new osg::MatrixTransform;
	mtH->addChild(nodoH);
	ss->addChild(mtH);
	ssCasaMax->addChild(mtH);
	ssPosada->addChild(mtH);
	ssCasaPueblo1->addChild(mtH);
	ssCasaPueblo2->addChild(mtH);
	ssCasaPueblo3->addChild(mtH);
	ssCasaPueblo4->addChild(mtH);
	/*las dos siguientes líneas harían que mtH y sus hijos no se vieran*/
	//mtH->setNodeMask( 0x2 ); //only 2nd bit set
	//viewer.getCamera()->setCullMask( 0x1 ); //2nd bit not set
	osg::Matrixd mtras, mrot, mrotz, mrotx, mroty, mesc, mtotal;
	Yo.setZ(obtenerAltura(Yo.getX(), Yo.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA));
	osg::Vec3d posH(Yo.getX(), Yo.getY(), Yo.getZ());
	mtras = mtras.translate(posH);
	mrotz = mrotz.rotate(Yo.getDireccion()+PI/2, osg::Vec3d(0, 0, 1));
	mesc = mesc.scale(.35, .35, .35);
	mtotal = mesc*mrot*mtras;
	mtH->setMatrix(mtotal);

	PRINT_CONTROL("MAX\n");
	osg::ref_ptr<osg::Node> nodoNPC = osgDB::readNodeFile("./Texturas/Humanos/Barman/trader_barman.obj");
	//nodoMax->setName("nodoMax");
	Max.setZ(obtenerAltura(Max.getX(), Max.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA));
	osg::ref_ptr<osg::MatrixTransform> mtMax = createMatrixTransform(Punto(Max.getPunto()), Max.getDireccion(), 1.0);
	mtMax->addChild(nodoNPC);
	mtMax->setName("mtMax");
	ss->addChild(mtMax);

	PRINT_CONTROL("JACK\n");
	//osg::ref_ptr<osg::Node> nodoJack = osgDB::readNodeFile("./Texturas/Humanos/Barman/trader_barman.obj");
	//nodoJack->setName("nodoJack");
	Jack.setZ(obtenerAltura(Jack.getX(), Jack.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA));
	osg::ref_ptr<osg::MatrixTransform> mtJack = createMatrixTransform(Punto(Jack.getPunto()), Jack.getDireccion(), 1.0);
	mtJack->addChild(nodoNPC);
	mtJack->setName("mtJack");
	ss->addChild(mtJack);

	PRINT_CONTROL("BARMAN\n");
	//osg::ref_ptr<osg::Node> nodoBarman = osgDB::readNodeFile("./Texturas/Humanos/Barman/trader_barman.obj");
	//nodoBarman->setName("nodoBarman");
	Barman.setZ(obtenerAltura(Barman.getX(), Barman.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA));
	osg::ref_ptr<osg::MatrixTransform> mtBarman = createMatrixTransform(Punto(Barman.getPunto()), Barman.getDireccion(), 1.0);
	mtBarman->addChild(nodoNPC);
	mtBarman->setName("mtBarman");
	ss->addChild(mtBarman);

	PRINT_CONTROL("JEFE\n");
	//osg::ref_ptr<osg::Node> nodoJefe = osgDB::readNodeFile("./Texturas/Humanos/Barman/trader_barman.obj");
	//nodoJefe->setName("nodoJefe");
	Jefe.setZ(obtenerAltura(Jefe.getX(), Jefe.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA));
	osg::ref_ptr<osg::MatrixTransform> mtJefe = createMatrixTransform(Punto(Jefe.getPunto()), Jefe.getDireccion(), 1.0);
	mtJefe->addChild(nodoNPC);
	mtJefe->setName("mtJefe");
	ss->addChild(mtJefe);

	PRINT_CONTROL("GUARDIA\n");
	//osg::ref_ptr<osg::Node> nodoGuardia = osgDB::readNodeFile("./Texturas/Humanos/Barman/trader_barman.obj");
	//nodoGuardia->setName("nodoGuardia");
	Guardia.setZ(obtenerAltura(Guardia.getX(), Guardia.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA));
	osg::ref_ptr<osg::MatrixTransform> mtGuardia = createMatrixTransform(Punto(Guardia.getPunto()), Guardia.getDireccion(), 1.0);
	mtGuardia->addChild(nodoNPC);
	mtGuardia->setName("mtGuardia");
	ss->addChild(mtGuardia);
	
	PRINT_CONTROL("CASA MAX\n");
	osg::ref_ptr<osg::Node> nodoCasaTroncos = osgDB::readNodeFile("./Texturas/Edificios/casa troncos/casa troncos.obj");
	//CasaMax.setZ(obtenerAltura(CasaMax.getX(), CasaMax.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA)-3);
	osg::ref_ptr<osg::MatrixTransform> mtCasaMax = createMatrixTransform(CasaMax.getPunto(), CASAMAX_DIR0*PI/180, ESCALA);
	mtCasaMax->addChild(nodoCasaTroncos);
	ssCasaMax->addChild(mtCasaMax);
	grupoEscenario->addChild(mtCasaMax);

	PRINT_CONTROL("POSADA\n");
	//osg::ref_ptr<osg::Node> nodoPosada = osgDB::readNodeFile("./Texturas/Edificios/casa troncos/casa troncos.obj");
	//Posada.setZ(obtenerAltura(Posada.getX(), Posada.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA)-3);
	osg::ref_ptr<osg::MatrixTransform> mtPosada = createMatrixTransform(Posada.getPunto(), POSADA_DIR0*PI/180, ESCALA);
	mtPosada->addChild(nodoCasaTroncos);
	ssPosada->addChild(mtPosada);
	ssPosada->addChild(mtBarman);
	ssPosada->addChild(mtJack);
	grupoEscenario->addChild(mtPosada);

	PRINT_CONTROL("CASA SEMICONSTRUIDA DEL PUEBLO \n");
	osg::ref_ptr<osg::Node> nodoCasaPuebloS = osgDB::readNodeFile("./Texturas/Edificios/casa troncos/casa troncos semiconstruida.obj");
	//CasaPuebloS.setZ(obtenerAltura(CasaPuebloS.getX(), CasaPuebloS.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA)-3);
	osg::ref_ptr<osg::MatrixTransform> mtCasaPuebloS = createMatrixTransform(CasaPuebloS.getPunto(), CASAPUEBLOS_DIR0*PI/180, ESCALA);
	mtCasaPuebloS->addChild(nodoCasaPuebloS);
	//ssCasaPueblo4->addChild(mtCasaPueblo4);
	grupoEscenario->addChild(mtCasaPuebloS);

	PRINT_CONTROL("CASA 4 DEL PUEBLO\n");
	//osg::ref_ptr<osg::Node> nodoCasaPueblo4 = osgDB::readNodeFile("./Texturas/Edificios/casa troncos/casa troncos.obj");
	//CasaPueblo4.setZ(obtenerAltura(CasaPueblo4.getX(), CasaPueblo4.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA)-3);
	osg::ref_ptr<osg::MatrixTransform> mtCasaPueblo4 = createMatrixTransform(CasaPueblo4.getPunto(), CASAPUEBLO4_DIR0*PI/180, ESCALA);
	mtCasaPueblo4->addChild(nodoCasaTroncos);
	ssCasaPueblo4->addChild(mtCasaPueblo4);
	grupoEscenario->addChild(mtCasaPueblo4);

	PRINT_CONTROL("CASA 3 DEL PUEBLO\n");
	//osg::ref_ptr<osg::Node> nodoCasaPueblo3 = osgDB::readNodeFile("./Texturas/Edificios/casa troncos/casa troncos.obj");
	//CasaPueblo3.setZ(obtenerAltura(CasaPueblo3.getX(), CasaPueblo3.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA)-3);
	osg::ref_ptr<osg::MatrixTransform> mtCasaPueblo3 = createMatrixTransform(CasaPueblo3.getPunto(), CASAPUEBLO3_DIR0*PI/180, ESCALA);
	mtCasaPueblo3->addChild(nodoCasaTroncos);
	ssCasaPueblo3->addChild(mtCasaPueblo3);
	grupoEscenario->addChild(mtCasaPueblo3);

	PRINT_CONTROL("CASA 2 DEL PUEBLO\n");
	//osg::ref_ptr<osg::Node> nodoCasaPueblo2 = osgDB::readNodeFile("./Texturas/Edificios/casa troncos/casa troncos.obj");
	//CasaPueblo2.setZ(obtenerAltura(CasaPueblo2.getX(), CasaPueblo2.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA)-3);
	osg::ref_ptr<osg::MatrixTransform> mtCasaPueblo2 = createMatrixTransform(CasaPueblo2.getPunto(), CASAPUEBLO2_DIR0*PI/180, ESCALA);
	mtCasaPueblo2->addChild(nodoCasaTroncos);
	ssCasaPueblo2->addChild(mtCasaPueblo2);
	grupoEscenario->addChild(mtCasaPueblo2);

	PRINT_CONTROL("CASA 1 DEL PUEBLO\n");
	//osg::ref_ptr<osg::Node> nodoCasaPueblo1 = osgDB::readNodeFile("./Texturas/Edificios/casa troncos/casa troncos.obj");
	//CasaPueblo1.setZ(obtenerAltura(CasaPueblo1.getX(), CasaPueblo1.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA)-3);
	osg::ref_ptr<osg::MatrixTransform> mtCasaPueblo1 = createMatrixTransform(CasaPueblo1.getPunto(), CASAPUEBLO1_DIR0*PI/180, ESCALA);
	mtCasaPueblo1->addChild(nodoCasaTroncos);
	ssCasaPueblo1->addChild(mtCasaPueblo1);
	grupoEscenario->addChild(mtCasaPueblo1);
	
	///*cargando*/texto_mover_ventana->setText("95%");
	///*cargando*/viewer.frame();

	PRINT_CONTROL("BOVEDA CELESTE\n");
	osg::ref_ptr<osg::Geode> geodeSky = new osg::Geode();
	osg::ref_ptr<osg::Image> imageSky = osgDB::readImageFile("./Texturas/Cielo/Sky_horiz_6-2048.jpg");
	//osg::ref_ptr<osg::Image> imageSky = osgDB::readImageFile("./Texturas/Cielo/skydome2-1024x1024.jpg");
	if (!imageSky)
		printf("\tImposible cargar textura cielo\n");
	osg::ref_ptr<osg::Texture2D> texture = new osg::Texture2D;
		texture->setDataVariance(osg::Object::DYNAMIC);
		texture->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR);
		texture->setFilter(osg::Texture::MAG_FILTER, osg::Texture::LINEAR);
		texture->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP_TO_EDGE);
		texture->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_EDGE);
		texture->setWrap(osg::Texture::WRAP_R, osg::Texture::CLAMP_TO_EDGE);
		texture->setImage(imageSky);
	osg::ref_ptr<osg::StateSet> state = geodeSky->getOrCreateStateSet();
	state->ref();
	osg::ref_ptr<osg::Material> material = new osg::Material();
		material->setEmission(osg::Material::FRONT, osg::Vec4(1.0, 1.0, 1.0, 1.0));
		material->setAmbient(osg::Material::FRONT, osg::Vec4(1.0, 1.0, 1.0, 1.0));
		material->setShininess(osg::Material::FRONT, 25.0);
	state->setAttribute(material);
	state->setTextureAttributeAndModes(0, texture, osg::StateAttribute::ON);
	texture->setImage(imageSky);
	
	geodeSky->addDrawable(new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(0, 0, 0), 1000)));

	osg::ref_ptr<osg::MatrixTransform> mtSky = new osg::MatrixTransform;
	grupoTotal->addChild(mtSky);
	mtSky->addChild(geodeSky);
	osg::Matrixd mtrasSky, mrotSky, mescSky, mtotalSky;
	mtrasSky = mtrasSky.translate(osg::Vec3d(0, 0, 0));
	mrotSky = mrotSky.rotate(-100, osg::Vec3d(-1, -1, 0));
	mescSky = mtrasSky.scale(1, 1, 1);
	mtotalSky = mescSky*mrotSky*mtrasSky;
	mtSky->setMatrix(mtotalSky);
	
	osg::ref_ptr<osg::Geode> geodeSpheres = new osg::Geode();
	grupoTotal->addChild(geodeSpheres);

	menu->resize();
	menu->resizePercent(10.0);


	////////////////////////////////////////SOMBRAS/////////////////////////////////////////////////
	PRINT_CONTROL("ADJUDICAR SOMBRAS\n");
	//quién da sombra
	//mtB->setNodeMask(CastsShadowTraversalMask);
	//mt de los árboles (ya está puesto en el código cuando se crean los árboles)

	//quién recibe sombra
	mtMalla->setNodeMask(ReceivesShadowTraversalMask);

	//quién da y recive sombras
	mtH->setNodeMask(ReceivesAndCastsShadowTraversalMask);
	mtMax->setNodeMask(ReceivesAndCastsShadowTraversalMask);
	mtJack->setNodeMask(ReceivesAndCastsShadowTraversalMask);
	mtBarman->setNodeMask(ReceivesAndCastsShadowTraversalMask);
	mtCasaMax->setNodeMask(ReceivesAndCastsShadowTraversalMask);
	
	//////////////////////////////////////preparar MUERTO///////////////////////////////////////////////////////////////////////////////////
	PRINT_CONTROL("PREPARAR ESTADO MUERTO\n"); 
	osg::ref_ptr<osgWidget::WindowManager> wmMuerto = new osgWidget::WindowManager(
		&viewer,
		double(windowLength),
		double(windowHeight),
		MASK_2D
	);

	osg::ref_ptr<osg::Camera> camera_muerto = wmMuerto->createParentOrthoCamera();
	camera_muerto->setProjectionMatrix(osg::Matrix::ortho2D(-400, 400, -300, 300));
	camera_muerto->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
	camera_muerto->setViewMatrix(osg::Matrix::identity());
	camera_muerto->setClearMask(GL_DEPTH_BUFFER_BIT);
	camera_muerto->setRenderOrder(osg::Camera::POST_RENDER);
	//camera_menu->addChild(wmMenu);
	osg::ref_ptr<osg::Group> grupoMuerto = new osg::Group;
	grupoMuerto->addChild(camera_muerto);

	
	//////////////////////////////////////preparar INTRODUCIR_NOMBRE///////////////////////////////////////////////////////////////////////////////////
	PRINT_CONTROL("PREPARAR INTRODUCIR NOMBRE\n"); 
	osg::ref_ptr<osgWidget::WindowManager> wmNombre = new osgWidget::WindowManager(
		&viewer,
		double(windowLength),
		double(windowHeight),
		MASK_2D//,
		//osgWidget::WindowManager::WM_PICK_DEBUG //esto sirve para mostrar la información de cada widget al pasar el ratón por encima
	);

	osg::ref_ptr<osgWidget::Window> boxVolver = new osgWidget::Box("BOXSONIDO", osgWidget::Box::VERTICAL);
	boxVolver->setAnchorHorizontal(osgWidget::Window::HA_CENTER);
	boxVolver->setY(windowHeight*0.2);
	osg::ref_ptr<VolverLabel> labelVolver = new VolverLabel("Volver");
	labelVolver->setSize(windowLength*0.1, windowHeight*0.05);
	boxVolver->addWidget(labelVolver);
	wmNombre->addChild(boxVolver);

	osg::ref_ptr<osgWidget::Box> boxNombre = new osgWidget::Box("BOXNOMBRE", osgWidget::Box::VERTICAL);
	osg::ref_ptr<osgWidget::Input> inputNombre = new osgWidget::Input("INPUTNOMBRE", "", 100);
	inputNombre->setFont("./Otros/fonts/VeraMono.ttf");
	inputNombre->setFontColor(0.0f, 0.0f, 0.0f, 1.0f);
	inputNombre->setFontSize(20);
	inputNombre->setYOffset(inputNombre->calculateBestYOffset("y"));
	inputNombre->setSize(400.0f, inputNombre->getText()->getCharacterHeight());
	boxNombre->addWidget(inputNombre);
	boxNombre->setOrigin(double(windowLength)/2-inputNombre->getSize().length()/2, double(windowHeight)/2);
	wmNombre->addChild(boxNombre);

	osg::ref_ptr<osg::Camera> camera_nombre = wmNombre->createParentOrthoCamera();
	camera_nombre->setProjectionMatrix(osg::Matrix::ortho2D(-400, 400, -300, 300));
	camera_nombre->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
	camera_nombre->setViewMatrix(osg::Matrix::identity());
	camera_nombre->setClearMask(GL_DEPTH_BUFFER_BIT);
	camera_nombre->setRenderOrder(osg::Camera::POST_RENDER);
	//camera_nombre->addChild(wmNombre); ya está añadido al crear camera_nombre
	osg::ref_ptr<osg::Group> grupoNombre = new osg::Group;
	grupoNombre->addChild(camera_nombre);

	osg::ref_ptr<osg::Geode> geode_texto_nombre3 = new osg::Geode();
	osg::ref_ptr<osgText::Text> texto_nombre3 = new osgText::Text;
	geode_texto_nombre3->addDrawable(texto_nombre3);
	camera_nombre->addChild(geode_texto_nombre3);
	texto_nombre3->setFont(fontArial);
	texto_nombre3->setPosition(osg::Vec3(double(windowLength)/2-inputNombre->getSize().length()/2, double(windowHeight)/2+inputNombre->getSize().y()*2, 0));
	texto_nombre3->setColor(osg::Vec4(255, 255, 255, 255));
	texto_nombre3->setFontResolution(40, 40);
	texto_nombre3->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_nombre3->setCharacterSize(20);
	texto_nombre3->setAlignment(osgText::Text::LEFT_CENTER);
	texto_nombre3->setText("Acuérdate de guardar cada vez que cumplas objetivos");

	osg::ref_ptr<osg::Geode> geode_texto_nombre2 = new osg::Geode();
	osg::ref_ptr<osgText::Text> texto_nombre2 = new osgText::Text;
	geode_texto_nombre2->addDrawable(texto_nombre2);
	camera_nombre->addChild(geode_texto_nombre2);
	texto_nombre2->setFont(fontArial);
	texto_nombre2->setPosition(osg::Vec3(double(windowLength)/2-inputNombre->getSize().length()/2, double(windowHeight)/2+inputNombre->getSize().y()*3.5, 0));
	texto_nombre2->setColor(osg::Vec4(255, 255, 255, 255));
	texto_nombre2->setFontResolution(40, 40);
	texto_nombre2->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_nombre2->setCharacterSize(20);
	texto_nombre2->setAlignment(osgText::Text::LEFT_CENTER);
	texto_nombre2->setText("Instrucciones: Nombre_PrimerApellido_SegundoApellido (sin tildes)");

	osg::ref_ptr<osg::Geode> geode_texto_nombre = new osg::Geode();
	osg::ref_ptr<osgText::Text> texto_nombre = new osgText::Text;
	geode_texto_nombre->addDrawable(texto_nombre);
	camera_nombre->addChild(geode_texto_nombre);
	texto_nombre->setFont(fontArial);
	texto_nombre->setPosition(osg::Vec3(double(windowLength)/2-inputNombre->getSize().length()/2, double(windowHeight)/2+inputNombre->getSize().y()*5, 0));
	texto_nombre->setColor(osg::Vec4(255, 255, 255, 255));
	texto_nombre->setFontResolution(40, 40);
	texto_nombre->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_nombre->setCharacterSize(20);
	texto_nombre->setAlignment(osgText::Text::LEFT_CENTER);
	texto_nombre->setText("Por favor, introduzce tu nombre y presiona enter");

	camera_nombre->addChild(geode_toast);
	
	camera_nombre->addChild(geode_mover_ventana);
	camera_texto->addChild(geode_mover_ventana);
	camera_muerto->addChild(geode_mover_ventana);

	//////////////////////////////////////preparar ESTADISTICAS///////////////////////////////////////////////////////////////////////////////////
	osg::ref_ptr<osg::Group> grupoEstadisticas = new osg::Group;

	PRINT_CONTROL("PREPARAR ESTADISTICAS\n");
	osg::ref_ptr<osgWidget::WindowManager> wmEstadisticas = new osgWidget::WindowManager(
		&viewer,
		double(windowLength),
		double(windowHeight),
		MASK_2D//,
			   //osgWidget::WindowManager::WM_PICK_DEBUG //esto sirve para mostrar la información de cada widget al pasar el ratón por encima
	);

	//osg::Camera* camera_menu = new osg::Camera;
	osg::ref_ptr<osg::Camera> camera_estadisticas = wmEstadisticas->createParentOrthoCamera();
	camera_estadisticas->setProjectionMatrix(osg::Matrix::ortho2D(-400, 400, -300, 300));
	camera_estadisticas->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
	camera_estadisticas->setViewMatrix(osg::Matrix::identity());
	camera_estadisticas->setClearMask(GL_DEPTH_BUFFER_BIT);
	camera_estadisticas->setRenderOrder(osg::Camera::POST_RENDER);
	//camera_menu->addChild(wmMenu);
	grupoEstadisticas->addChild(camera_estadisticas);

	camera_estadisticas->addChild(geode_mover_ventana);

	osg::ref_ptr<osgWidget::Box> boxEstadisticas = new osgWidget::Box("BOXESTADISTICAS", osgWidget::Box::VERTICAL);
	//boxEstadisticas->setAnchorVertical(osgWidget::Window::VA_TOP);
	boxEstadisticas->setY(windowHeight*0.3);
	boxEstadisticas->setAnchorHorizontal(osgWidget::Window::HA_CENTER);
	std::vector<osg::ref_ptr<RespuestaLabel>> vectorEstadisticas;
	for (int i = 10; i >= 0; i--) {
		osg::ref_ptr<RespuestaLabel> estadistica;
		if (i == 0)
			estadistica = new RespuestaLabel(i, "", windowLength*0.4, windowHeight*0.06);
		else
			estadistica = new RespuestaLabel(i, "", windowLength*0.4, windowHeight*0.04);
		vectorEstadisticas.push_back(estadistica);
		boxEstadisticas->addWidget(estadistica);
	}
	boxEstadisticas->getBackground()->setColor(COLOR_GRIS_OSCURO);
	wmEstadisticas->addChild(boxVolver);
	wmEstadisticas->addChild(boxEstadisticas);


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	///*cargando*/texto_mover_ventana->setText("100%");
	///*cargando*/viewer.frame();
	timer.printTime(); //tarda en cargar: unos 6-7s
	texto_mover_ventana->setText("mueve la ventana");
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	while(!viewer.done()){

			switch (ETAPA_GENERAL) {

				case MENU_INICIO:
				{
						osg::ref_ptr<osgWidget::MouseHandler> menuMouseHandler = new osgWidget::MouseHandler(wmMenu);
						osg::ref_ptr<osgWidget::ResizeHandler> menuResizeHandler = new osgWidget::ResizeHandler(wmMenu, camera_menu);
						viewer.addEventHandler(menuMouseHandler);
						viewer.addEventHandler(menuResizeHandler);
						boxMenu->resize();
						wmMenu->resizeAllWindows();
						viewer.setSceneData(grupoMenu);
						viewer.realize();
						while (ETAPA_GENERAL == MENU_INICIO && !viewer.done()) {
							if (label1->clicado) {
								ETAPA_GENERAL = INSERTAR_NOMBRE;
								label1->reiniciarClick();
							}
							else if (label2->clicado){
								ETAPA_GENERAL = INSERTAR_CARGAR_NOMBRE;
								label2->reiniciarClick();
							}
							else if (label3->clicado){
								ETAPA_GENERAL = ESTADISTICAS;
								label3->reiniciarClick();
							}
							else if (label4->clicado){
								ETAPA_GENERAL = MENU_INICIO;
								label4->reiniciarClick();
							}
							else if (label5->clicado){
								ETAPA_GENERAL = SALIR;
								label5->reiniciarClick();
							}

							Sleep(10);
							viewer.frame();
						}
						//viewer.setDone(false);
						viewer.removeEventHandler(menuResizeHandler);
						viewer.removeEventHandler(menuMouseHandler);
						break;
				}
				case INSERTAR_CARGAR_NOMBRE:
				case INSERTAR_NOMBRE:
				{
						bool enterPulsado = false;
						osg::ref_ptr<osgWidget::ResizeHandler> nombreResizeHandler = new osgWidget::ResizeHandler(wmNombre, camera_nombre);
						osg::ref_ptr<osgWidget::MouseHandler> nombreMouseHandler = new osgWidget::MouseHandler(wmNombre);
						osg::ref_ptr<osgWidget::KeyboardHandler> nombreKeyboardHandler = new osgWidget::KeyboardHandler(wmNombre);
						osg::ref_ptr<NameInsertedHandler> nameInsertedHandler = new NameInsertedHandler(65293, inputNombre, NOMBRE, enterPulsado);
						viewer.addEventHandler(nombreResizeHandler);
						viewer.addEventHandler(nombreMouseHandler);
						viewer.addEventHandler(nombreKeyboardHandler);
						viewer.addEventHandler(nameInsertedHandler);

						if(ETAPA_GENERAL == INSERTAR_NOMBRE)
							texto_nombre->setText("Por favor, introduzce tu nombre y presiona enter");
						else if (ETAPA_GENERAL == INSERTAR_CARGAR_NOMBRE)
							texto_nombre->setText("Por favor, introduzce tu nombre de la partida guardada y presiona enter");

						wmNombre->resizeAllWindows();
						viewer.setSceneData(grupoNombre);
						viewer.realize();
						while ( (ETAPA_GENERAL == INSERTAR_NOMBRE || ETAPA_GENERAL == INSERTAR_CARGAR_NOMBRE) && !viewer.done()) {
							if (labelVolver->clicado) {
								ETAPA_GENERAL = MENU_INICIO;
								//boxSonido->resize();
								labelVolver->reiniciarClick();
							}
							if (enterPulsado) {
								if (NOMBRE.length() >= 1) {
									std::string nombre_archivo = "./PartidasGuardadas/" + NOMBRE + ".xml";
									if (fileExists(nombre_archivo)) {
										//static bool a = false;
										//if (a == false) {
											if (ETAPA_GENERAL == INSERTAR_NOMBRE) {
												sentencia->setLabel(ajustarString(std::string("Ya existe una partida guardada con ese nombre, ¿deseas cargarla?"), 30));
												opcionAceptar->reiniciarClick();
												opcionCancelar->reiniciarClick();
												boxConfirmar->setY(windowHeight*0.25);
												wmNombre->addChild(boxConfirmar);
												enterPulsado = false;
												while (!opcionAceptar->clicado && !opcionCancelar->clicado && !enterPulsado && !viewer.done()) {
													Sleep(10);
													viewer.frame();
												}
												if (opcionAceptar->clicado || enterPulsado) {
													ETAPA_GENERAL = CARGAR;
												}
												else if (opcionCancelar->clicado) {
													toast_global.addMessage("Por favor, introduce otro nombre", 3);
												}
												wmNombre->removeChild(boxConfirmar);
											}
											else if (ETAPA_GENERAL == INSERTAR_CARGAR_NOMBRE) {
												std::string saludo = "Bienvenido " + NOMBRE + "\n";
												toast_global.addMessage(saludo, 3);
												//a = true;
												ETAPA_GENERAL = CARGAR;
											}
										//}
									}
									else {
										if (ETAPA_GENERAL == INSERTAR_NOMBRE) {
											std::string saludo = "Bienvenido " + NOMBRE + "\n";
											toast_global.addMessage(saludo, 3);
											//a = true;
											ETAPA_GENERAL = REINICIAR_PARTIDA;
										}
										else if (ETAPA_GENERAL == INSERTAR_CARGAR_NOMBRE) {
											std::string error = "No existe ninguna partida guardada con ese nombre\n";
											toast_global.addMessage(error, 3);
										}
									}
									
								}
								else {
									std::string peticion = "Por favor, introduce algún caracter\n";
									toast_global.addMessage(peticion, 3);
								}
								enterPulsado = false;
							}
							texto_toast->setText(toast_global.getMessages());
							Sleep(10);
							viewer.frame();
						}
						viewer.removeEventHandler(nombreResizeHandler);
						viewer.removeEventHandler(nombreMouseHandler);
						viewer.removeEventHandler(nombreKeyboardHandler);
						viewer.removeEventHandler(nameInsertedHandler);
						break;
				}
				case JUGAR:
				{		
						chunk_countryside.play(-1);
						labelSonido->setOn();

						std::string pickedPath;
						osg::ref_ptr<SimulacionKeyboardEvent> KeyboardEvent = new SimulacionKeyboardEvent();
						osg::ref_ptr<osgWidget::MouseHandler> mouseHandler = new osgWidget::MouseHandler(wm);
						osg::ref_ptr<osgWidget::ResizeHandler> resizeHandler = new osgWidget::ResizeHandler(wm, camera_texto);
						osg::ref_ptr<PickHandler> pickHandler = new PickHandler(pickedPath);
						viewer.addEventHandler(KeyboardEvent);
						viewer.addEventHandler(mouseHandler);
						viewer.addEventHandler(resizeHandler);
						viewer.addEventHandler(pickHandler);
						
						labelMenu->reiniciarClick();

						double t1 = 0, t0 = 0;
						viewer.setSceneData(grupoTotal);
						viewer.realize();
						tiempoCompletado = 0;
						double esfuerzo = 0;
						t1 = ::GetCurrentTime();//sino se pone esta línea, el primer t1-t0 es muy grande
						while (ETAPA_GENERAL == JUGAR && !viewer.done()) {

							t0 = t1;
							t1 = ::GetCurrentTime();
							double cycleTime = (t1 - t0) / 1000;

							tiempoCompletado += cycleTime; //tiempo completado en segundos
							int tiempoRestante = TOTAL_TIME - tiempoCompletado + tiempoPerdido;
							//widgetNumero1->setNumero(tiempoRestante/60/10%10);		//decenas de minutos
							//widgetNumero2->setNumero(tiempoRestante/60%10);  //unidades de minutos
							//widgetNumeroP->setNumero('p');
							//widgetNumero3->setNumero(tiempoRestante%60/10%10); //decenas de segundos
							//widgetNumero4->setNumero(tiempoRestante%60%10); //unidades de segundos
							/*std::string st = "Te quedan\n" + std::to_string(int(tiempoRestante)) + "s";
							texto_tiempo->setText(st);*/
							//if (tiempoRestante) {}	//falta

							
							{
								//youAreAt(Yo.getX(), Yo.getY(), imagenMapa, NUM_PUNTOS); //dibuja un punto en el mapa donde se encuentre el personaje
								//static double x=Yo.getX();
								//static double y=Yo.getY();
								//youAreNotAt2(x, y, imagenMapa, imagenMapa2); //restaura los pixeles de imagenMapa2 anteriormente cambiados
								//x = Yo.getX();
								//y = Yo.getY();
								//youAreAt2(x, y, Yo.getDireccion(), imagenCursor, imagenMapa2); //dibuja un cursor en imagenMapa2 donde se encuentre el personaje
								//widgetMap->setImage(imagenMapa2);
								static double x = Yo.getX();
								static double y = Yo.getY(); 
								mapa1.desdibujarFlecha(x, y);
								x = Yo.getX();
								y = Yo.getY();
								mapa1.dibujarFlecha(x,y,Yo.getDireccion());
								widgetMap->setImage(mapa1.getImagen());
							}

							if (FASE_JUEGO == "0") {
								if (!ventanaAñadida) {
									wm->addChild(boxVentana);
									ventanaAñadida = true;
									ventana->setLabel("");
									ventana->setColor(osg::Vec4(1.0f, 0.0f, 0.0f, 0.0f));
									boxVentana->getBackground()->setColor(osg::Vec4(0, 0, 0, 0));
								}
								if (tiempoCompletado > 20) {
									if (Yo.getCorrer())
										esfuerzo += 0.010;
									else if(Yo.enMovimiento())
										esfuerzo += 0.005;
									esfuerzo += cycleTime/15;
									ventana->setColor(osg::Vec4(1.0f, 0.0f, 0.0f, esfuerzo));
									boxVentana->getBackground()->setColor(osg::Vec4(esfuerzo, esfuerzo, esfuerzo, esfuerzo));
									if (esfuerzo >= 1.2)
										ventana->setLabel("Te has desmayado");
									if (esfuerzo >= 1.5) {
										FASE_JUEGO = "1";
										wm->removeChild(boxVentana);
										ventana->setLabel("");
										ventanaAñadida = false;
										Yo.setPosition(YO_X1, YO_Y1, YO_Z1, YO_DIRECCION1*PI/180.0);
										toast_global.addMessage("Pulsa en Ayuda del Menu si necesitas instrucciones sobre el juego", 5);
										toast_global.addMessageWithKey("Pulsa 'w' dos veces seguidas para correr", 'w');
									}
								}
							}

							////////menú de la esquina superior derecha
							if (labelMenu->c1->clicado) {//Empezar de nuevo
								ETAPA_GENERAL = REINICIAR_PARTIDA;
								labelMenu->_window->hide();
								labelMenu->reiniciarClick(1);
							}
							if (labelMenu->c2->clicado) {//Guardar
								std::string nombre_archivo = "./PartidasGuardadas/" + NOMBRE + ".xml";
								if (fileExists(nombre_archivo)) {
									sentencia->setLabel(ajustarString(std::string("Ya existe una partida guardada con tu nombre, deseas sobreescribirla?"),20));
									opcionAceptar->reiniciarClick();
									opcionCancelar->reiniciarClick();
									boxConfirmar->setAnchorVertical(osgWidget::Window::VA_CENTER);
									wm->addChild(boxConfirmar);
									double tt = ::GetCurrentTime();
									while ( !opcionAceptar->clicado && !opcionCancelar->clicado && !viewer.done()) {
										Sleep(10);
										viewer.frame();
									}
									if (opcionAceptar->clicado) {
										ETAPA_GENERAL = GUARDAR;
										labelMenu->_window->hide();
									}
									else if (opcionCancelar->clicado) {
										labelMenu->_window->hide();
									}
									tiempoPerdido = ::GetCurrentTime() - tt;
									wm->removeChild(boxConfirmar);
								}
								else {
									ETAPA_GENERAL = GUARDAR;
									labelMenu->_window->hide();
								}
								labelMenu->reiniciarClick(2);
							}
							if (labelMenu->c3->clicado) {//Volver a inicio sin guardar
								ETAPA_GENERAL = MENU_INICIO;
								labelMenu->_window->hide();
								labelMenu->reiniciarClick(3);
							}
							if (labelMenu->c4->clicado) {//Salir sin guardar
								exit(0);
								labelMenu->_window->hide();
								labelMenu->reiniciarClick(4);
							}
							if (labelMenu->c5->clicado) {//Ayuda
								if (!ayudaMostrada) {
									wm->addChild(boxAyuda);
									ayudaMostrada = true;
								}
								else {
									wm->removeChild(boxAyuda);
									ayudaMostrada = false;
									labelMenu->_window->hide();
								}
								labelMenu->reiniciarClick(5);
							}
							//botón de activar o desactivar el sonido en la esquina superior izquierda
							if (labelSonido->clicado) {
								if (chunk_countryside.isPaused()) {
									labelSonido->setOn();
									chunk_countryside.resume();
									printf("poniendo sonido\n");
								}
								else{
									labelSonido->setOff();
									chunk_countryside.pause();
									printf("quitando sonido\n");
								}
								boxSonido->resize();
								labelSonido->reiniciarClick();
							}

							////////diálogos
							{
								static bool dialogosAñadidos = false;
								GuardaDialogos *dialogos;
								double distanciaAMax = Yo.distanciaA(Max.getPunto());
								double distanciaAJack = Yo.distanciaA(Jack.getPunto());
								double distanciaABarman = Yo.distanciaA(Barman.getPunto());
								double distanciaAJefe = Yo.distanciaA(Jefe.getPunto());
								double distanciaAGuardia = Yo.distanciaA(Guardia.getPunto());
								//girar al personaje que está cerca para que mire a Yo
								{
									Punto p1 = Yo.getPunto();
									if (distanciaAMax < DISTANCIA_MIN_A_OTROS) {
										Punto p2 = Max.getPunto();
										double angulo = atan2(p1.getY() - p2.getY(), p1.getX() - p2.getX());
										Max.setDireccion(angulo);
										mtMax->setMatrix((createMatrixTransform(Punto(Max.getPunto()), Max.getDireccion(), 1.0))->getMatrix());
									}
									if (distanciaAJack < DISTANCIA_MIN_A_OTROS) {
										Punto p2 = Jack.getPunto();
										double angulo = atan2(p1.getY() - p2.getY(), p1.getX() - p2.getX());
										Jack.setDireccion(angulo);
										mtJack->setMatrix((createMatrixTransform(Punto(Jack.getPunto()), Jack.getDireccion(), 1.0))->getMatrix());
									}
									if (distanciaABarman < DISTANCIA_MIN_A_OTROS) {
										Punto p2 = Barman.getPunto();
										double angulo = atan2(p1.getY() - p2.getY(), p1.getX() - p2.getX());
										Barman.setDireccion(angulo);
										mtBarman->setMatrix((createMatrixTransform(Punto(Barman.getPunto()), Barman.getDireccion(), 1.0))->getMatrix());
									}
									if (distanciaAJefe < DISTANCIA_MIN_A_OTROS) {
										Punto p2 = Jefe.getPunto();
										double angulo = atan2(p1.getY() - p2.getY(), p1.getX() - p2.getX());
										Jefe.setDireccion(angulo);
										mtJefe->setMatrix((createMatrixTransform(Punto(Jefe.getPunto()), Jefe.getDireccion(), 1.0))->getMatrix());
									}
									if (distanciaAGuardia < DISTANCIA_MIN_A_OTROS) {
										Punto p2 = Guardia.getPunto();
										double angulo = atan2(p1.getY() - p2.getY(), p1.getX() - p2.getX());
										Guardia.setDireccion(angulo);
										mtGuardia->setMatrix((createMatrixTransform(Punto(Guardia.getPunto()), Guardia.getDireccion(), 1.0))->getMatrix());
									}
								}
								//buscar el diálogo del personaje que esté más cerca
								double min = min(distanciaAMax, distanciaAJack);
								min = min(min, distanciaABarman);
								min = min(min, distanciaAJefe);
								min = min(min, distanciaAGuardia);
								if (min < DISTANCIA_MIN_A_OTROS) {
									if (min == distanciaAMax)
										dialogos = &dialogosMax;
									else if (min == distanciaAJack)
										dialogos = &dialogosJack;
									else if (min == distanciaABarman)
										dialogos = &dialogosBarman;
									else if (min == distanciaAJefe)
										dialogos = &dialogosJefe;
									else if (min == distanciaAGuardia)
										dialogos = &dialogosGuardia;
									

									for (int i = 0; i < dialogos->numDialogos; i++) {
										if ( dialogos->dialogos[i].fase==FASE_JUEGO && dialogos->dialogos[i].personaje==dialogos->nombre && !(dialogoPersonaje==dialogos->dialogos[i].personaje && dialogoFase==dialogos->dialogos[i].fase)) {
											//diaglogoMostrado = &(dialogos->dialogos[i]);
											dialogoPersonaje = dialogos->nombre;
											dialogoFase = dialogos->dialogos[i].fase;
											dialogoDa = dialogos->dialogos[i].pregunta.da;
											dialogoConRespuesta = dialogos->dialogos[i].pregunta.conrespuesta;
											dialogoNecesario1 = dialogos->dialogos[i].respuesta[0].necesario;
											dialogoNecesario2 = dialogos->dialogos[i].respuesta[1].necesario;
											dialogoNecesario3 = dialogos->dialogos[i].respuesta[2].necesario;
											dialogoSino1 = (dialogos->dialogos[i].respuesta[0]).sino;
											dialogoSino2 = (dialogos->dialogos[i].respuesta[1]).sino;
											dialogoSino3 = (dialogos->dialogos[i].respuesta[2]).sino;
											boxDialogo->removeWidget(pregunta); //se quita la pregunta de boxDialogo y luego se añade ya que hay que añadir empezar a añadir por el último
											//respuesta3
											std::string s = dialogos->dialogos[i].respuesta[2].respuesta;
											if (s.length() == 0) //añadir la respuesta3 si tiene algo de texto
												boxDialogo->removeWidget(respuesta3);
											else {
												respuesta3->setLabel(ajustarString(s, MAX_POR_LINEA));
												boxDialogo->addWidget(respuesta3);
											}
											//respuesta2
											s = dialogos->dialogos[i].respuesta[1].respuesta;
											if (s.length() == 0) //añadir la respuesta2 si tiene algo de texto
												boxDialogo->removeWidget(respuesta2);
											else {
												respuesta2->setLabel(ajustarString(s, MAX_POR_LINEA));
												boxDialogo->addWidget(respuesta2);
											}
											//respuesta1
											s = dialogos->dialogos[i].respuesta[0].respuesta;
											if (s.length() == 0) //añadir la respuesta1 si tiene algo de texto (esta lo tiene que tener)
												boxDialogo->removeWidget(respuesta1);
											else {
												respuesta1->setLabel(ajustarString(s, MAX_POR_LINEA));
												boxDialogo->addWidget(respuesta1);
											}
											//pregunta
											s = dialogoPersonaje + ": " + dialogos->dialogos[i].pregunta.pregunta;
											pregunta->setLabel(ajustarString(s, MAX_POR_LINEA));
											boxDialogo->addWidget(pregunta);
											//a qué fase hay que pasar si se selecciona la respuesta correspondiente
											respuesta1fase = dialogos->dialogos[i].respuesta[0].siguientefase;
											respuesta2fase = dialogos->dialogos[i].respuesta[1].siguientefase;
											respuesta3fase = dialogos->dialogos[i].respuesta[2].siguientefase;
											boxDialogo->resize();
											//break; //¿esto se sale solo del if, del for o de algo más?
										}
									}
									//añadir o no el cuadro de diálogos
									if (!dialogosAñadidos && dialogoPersonaje == dialogos->nombre && dialogoFase == FASE_JUEGO) {
										wm->addChild(boxDialogo);
										dialogosAñadidos = true;
									}
									else if (dialogosAñadidos && ( dialogoPersonaje != dialogos->nombre || dialogoFase != FASE_JUEGO )) {
										wm->removeChild(boxDialogo);
										dialogosAñadidos = false;
									}
									//comprobar si se ha hecho click en alguna respuesta
									std::string faseSiguiente;
									if (respuesta1->clicado) {
										if (dialogoConRespuesta == 1 && dialogoDa != "nada") {//obtener lo que le da el personaje si el jugador ha elegido esta respuesta
											size_t found = dialogoDa.find("mapa");
											if (found != std::string::npos)
												toast_global.addMessageWithKey("Pulsa 'm' para agrandar o reducir el mapa", 'm');
											descodificarYobtenerObjetos(Yo.getInventario(), dialogoDa);
											Yo.setInventarioCambiado(true);
										}
										//comprobar si necesita algún objeto para elegir esta respuesta
										if (dialogoNecesario1 != "nada") {
											if (Yo.poseeObjeto(Objeto(dialogoNecesario1))) {
												printf("\n\nquitando objeto:%s,terminado\n\n", dialogoNecesario1.c_str());
												Yo.eliminarObjeto(Objeto(dialogoNecesario1));
												Yo.setInventarioCambiado(true);
												faseSiguiente = respuesta1fase;
												FASE_JUEGO = respuesta1fase;
											}
											else {
												faseSiguiente = dialogoSino1;
												FASE_JUEGO = dialogoSino1;
											}
										}
										else {
											faseSiguiente = respuesta1fase;
											FASE_JUEGO = respuesta1fase;
										}
										/*faseSiguiente = respuesta1fase;
										FASE_JUEGO = respuesta1fase;*/
										respuesta1->reiniciarClick();
										toast_global.addMessage("¿Hora de guardar?", 3);
									}
									else if (respuesta2->clicado) {
										if (dialogoConRespuesta == 2 && dialogoDa != "nada") {//obtener lo que le da el personaje si el jugador ha elegido esta respuesta
											size_t found = dialogoDa.find("mapa");
											if (found != std::string::npos)
												toast_global.addMessage("Pulsa 'm' para agrandar o reducir el mapa", 10);
											descodificarYobtenerObjetos(Yo.getInventario(), dialogoDa);
											Yo.setInventarioCambiado(true);
										}
										//comprobar si necesita algún objeto para elegir esta respuesta
										if (dialogoNecesario2 != "nada") {
											if (Yo.poseeObjeto(Objeto(dialogoNecesario2))) {
												Yo.eliminarObjeto(Objeto(dialogoNecesario2));
												faseSiguiente = respuesta2fase;
												FASE_JUEGO = respuesta2fase;
											}
											else {
												faseSiguiente = dialogoSino2;
												FASE_JUEGO = dialogoSino2;
											}
										}
										else {
											faseSiguiente = respuesta2fase;
											FASE_JUEGO = respuesta2fase;
										}
										/*faseSiguiente = respuesta2fase;
										FASE_JUEGO = respuesta2fase;*/
										respuesta2->reiniciarClick();
										toast_global.addMessage("¿Hora de guardar?", 3);
									}
									else if (respuesta3->clicado) {
										if (dialogoConRespuesta == 3 && dialogoDa != "nada") {//obtener lo que le da el personaje si el jugador ha elegido esta respuesta
											size_t found = dialogoDa.find("mapa");
											if (found != std::string::npos)
												toast_global.addMessage("Pulsa 'm' para agrandar o reducir el mapa", 10);
											descodificarYobtenerObjetos(Yo.getInventario(), dialogoDa);
											Yo.setInventarioCambiado(true);
										}
										//comprobar si necesita algún objeto para elegir esta respuesta
										if (dialogoNecesario3 != "nada") {
											if (Yo.poseeObjeto(Objeto(dialogoNecesario3))) {
												Yo.eliminarObjeto(Objeto(dialogoNecesario3));
												faseSiguiente = respuesta3fase;
												FASE_JUEGO = respuesta3fase;
											}
											else {
												faseSiguiente = dialogoSino3;
												FASE_JUEGO = dialogoSino3;
											}
										}
										else {
											faseSiguiente = respuesta3fase;
											FASE_JUEGO = respuesta3fase;
										}
										/*faseSiguiente = respuesta3fase;
										FASE_JUEGO = respuesta3fase;*/
										respuesta3->reiniciarClick();
										toast_global.addMessage("¿Hora de guardar?", 3);
									}
									if (faseSiguiente[0] == 'M') {
													
													chunk_gun.play(1);

													wm->addChild(boxVentana);
													ventana->setColor(osg::Vec4(1.0f, 0.0f, 0.0f, 0.0f));
													boxVentana->getBackground()->setColor(osg::Vec4(0, 0, 0, 0));

													double color = 0;
													while (color <= 1.0 && !viewer.done()) {
														color += 0.02;
														ventana->setColor(osg::Vec4(1.0f, 0.0f, 0.0f, color));
														boxVentana->getBackground()->setColor(osg::Vec4(color, color, color, color));
														Sleep(10);
														viewer.frame();
													}
													ventana->setLabel(porqueEstoyMuerto(faseSiguiente));
													while (color <= 2.0 && !viewer.done()) {
														color += 0.005;
														Sleep(10);
													}

													opcionAceptar->setLayer(osgWidget::Widget::LAYER_TOP);
													opcionCancelar->setLayer(osgWidget::Widget::LAYER_TOP);
													ventana->setLayer(osgWidget::Widget::LAYER_BG);
													sentencia->setLabel(ajustarString(std::string("Estás muerto.\n¿Deseas volver al menú de inicio?"), 30));
													opcionAceptar->reiniciarClick();
													opcionCancelar->reiniciarClick();
													boxConfirmar->setY(windowHeight*0.25);
													wm->addChild(boxConfirmar);
													while (ETAPA_GENERAL==JUGAR && !viewer.done()) {
														if (opcionAceptar->clicado) {
															ETAPA_GENERAL = MENU_INICIO;
															opcionAceptar->reiniciarClick();
														}
														else if (opcionCancelar->clicado) {
															toast_global.addMessage("Por favor, haz click en Aceptar", 3);
															opcionCancelar->reiniciarClick();
														}

														Sleep(10);
														viewer.frame();
													}

													wm->removeChild(boxConfirmar);
													wm->removeChild(boxVentana);
									}
								}
								else {
									if (dialogosAñadidos == true) {
										wm->removeChild(boxDialogo);
										dialogosAñadidos = false;
									}
								}
								
								respuesta1->clicado = false;
								
							}
							///////////

							if (Yo.getInventarioCambiado()) {
								printf("Aqui muestro inventario\n");
								wm->removeChild(boxInventario);
								boxInventario = mostrarInventario(Yo.getInventario());
								wm->addChild(boxInventario);
								Yo.setInventarioCambiado(false);
							}

							if (!yoTieneMapa) {
								if (Yo.poseeObjeto(Objeto(std::string("mapa")))) {
									wm->addChild(boxMap);
									yoTieneMapa = true;
								}
							}

							double var = 45 * PI / 180;
							static unsigned int colision[8];
							for (int i = 0; i < 8; i++) {
								colision[i] = 0;
							}
							//geodeSpheres->removeDrawables(0, 8); //para ver las colisiones
							for (int i = 0; i < 8; i++) {
								osg::ref_ptr<osg::LineSegment> segment = new osg::LineSegment();
								segment->set(
									osg::Vec3(Yo.getX(), Yo.getY(), Yo.getZ() + 1),
									osg::Vec3(Yo.getX() + LONGITUD_RAYO*cos(Yo.getDireccion() + i*var), Yo.getY() + LONGITUD_RAYO*sin(Yo.getDireccion() + i*var), Yo.getZ() + 1));
								//geodeSpheres->addDrawable(new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(Yo.getX() + LONGITUD_RAYO*cos(Yo.getDireccion()+i*var), Yo.getY() + LONGITUD_RAYO*sin(Yo.getDireccion()+i*var), Yo.getZ() + 1), 0.05)));  //para ver las colisiones
								osgUtil::IntersectVisitor intersector;
								intersector.addLineSegment(segment);
								grupoEscenario->accept(intersector);
								osgUtil::IntersectVisitor::HitList hitList = intersector.getHitList(segment);
								if (!hitList.empty()) {
									colision[i] = 1;
								}
							}

							Yo.actua(cycleTime, imagen_heightmap, colision);
							posH.set(Yo.getX(), Yo.getY(), Yo.getZ());
							mtras = mtras.translate(posH);
							mrotz = mrotz.rotate(Yo.getDireccion() + PI / 2, osg::Vec3d(0, 0, 1));
							mesc = mtras.scale(.35, .35, .35);
							mtotal = mesc*mrotz*mtras;
							mtH->setMatrix(mtotal);

							calcularPosicionCamara();
							colocarCamara();

							Sol.move(cycleTime);
							myLight0->setPosition(osg::Vec4(Sol.getX(), Sol.getY(), Sol.getZ(), 1.0f));
							geodeSol->removeDrawables(0, 1);
							geodeSol->addDrawable(new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(1.1*Sol.getX(), 1.1*Sol.getY(), 1.1*Sol.getZ()), 10)));//para ver de dónde viene la luz

							{
							 //std::stringstream ss;
							 //ss << "x=" << Yo.getX() << ", y=" << Yo.getY() << ", z=" << Yo.getZ();
							 //texto_intro2->setText(ss.str());
							 //no podemos usar stream
								/*std::string s = "";
								s.append("Yo: x=");
								s.append(std::to_string(Yo.getX()));
								s.append(", y=");
								s.append(std::to_string(Yo.getY()));
								s.append(", z=");
								s.append(std::to_string(Yo.getZ()));
								s.append(", dir=");
								s.append(std::to_string(Yo.getDireccion()));
								s.append("\ncolisiones: ");
								for (int i = 0; i < 8; i++) {
									s.append(std::to_string(colision[i]));
								}
								s.append("\ntiempo de ciclo: ");
								s.append(std::to_string(int(cycleTime*1000)));
								s.append("ms\n");
								s.append(str_duracionDia);
								s.append("\nfase del juego: ");
								s.append(FASE_JUEGO);
								if(FASE_JUEGO!="0")
									texto_intro2->setText(s);
								else
									texto_intro2->setText("");*/
								texto_toast->setText(toast_global.getMessages());
							}
							//texto_intro3->setText(str_coordMouse);

							//ajustar los Widgets si se cambia el tamaño de la pantalla
							/*{
								int length, height;
								getWindowSize(viewer, length, height);
								if ( length != windowLength || height != windowHeight) {
									windowLength = length;
									windowHeight = height;
									wm->setWidth(windowLength);
									wm->setHeight(windowHeight);
									wm->setWindowSize(windowLength, windowHeight);
									wm->resizeAllWindows();
									menu->setAnchorVertical(osgWidget::Window::VA_TOP);
									menu->setAnchorHorizontal(osgWidget::Window::HA_RIGHT);
									menu->resize();
									menu->update();
									printf("pantalla: %d x %d\n", windowLength, windowHeight);
								}
							}*/

							{//obtener el nombre de un personaje al clicar en él
								if (!pickedPath.empty()) {
									if (pickedPath.find("Jack") != std::string::npos) {
										toast_global.addMessage("Este es Jack", 1);
									}
									else if (pickedPath.find("Max") != std::string::npos) {
										toast_global.addMessage("Este es Max", 1);
									}
									else if (pickedPath.find("Barman") != std::string::npos) {
										toast_global.addMessage("Este es el Barman", 1);
									}
									else if (pickedPath.find("Jefe") != std::string::npos) {
										toast_global.addMessage("Este es el Jefe del Poblado", 1);
									}
									else if (pickedPath.find("Guardia") != std::string::npos) {
										toast_global.addMessage("Este es el Guardia del Poblado", 1);
									}
									pickedPath.clear();
								}
							}


							Sleep(10);
							viewer.frame();

						}
						viewer.removeEventHandler(KeyboardEvent);
						viewer.removeEventHandler(resizeHandler);
						viewer.removeEventHandler(mouseHandler);
						viewer.removeEventHandler(pickHandler);
						break;
				}
				case GUARDAR:
				{
					ArchivosGuardar datosGuardar;
					datosGuardar.posX = Yo.getX();
					datosGuardar.posY = Yo.getY();
					datosGuardar.posZ = Yo.getZ();
					datosGuardar.dir = Yo.getDireccion();
					Inventario* mochilaYo = Yo.getInventario();
					datosGuardar.listaYo = mochilaYo->getLista();
					datosGuardar.scroll_times = scroll_times;
					datosGuardar.anguloCamaraVertical0 = anguloCamaraVertical0;
					datosGuardar.anguloCamaraHorizontal0 = anguloCamaraHorizontal0;
					datosGuardar.anguloCamaraHorizontal_1aPersona = anguloCamaraHorizontal_1aPersona;
					datosGuardar.anguloCamaraVertical_1aPersona = anguloCamaraVertical_1aPersona;
					datosGuardar.FASE_JUEGO = FASE_JUEGO;
					datosGuardar.FASE_JUEGOprev = FASE_JUEGOprev;
					datosGuardar.NOMBRE = NOMBRE;
					datosGuardar.tiempoCompletado = tiempoCompletado;
					datosGuardar.thetaSol = Sol.getTheta();
					datosGuardar.phiSol = Sol.getPhi();

					std::string partidaGuardada = "partidaGuardada";
					Guardar(datosGuardar, partidaGuardada);

					ETAPA_GENERAL = JUGAR;
					break;
				}
				case CARGAR:
				{
					ArchivosGuardar archivosCargar;
					archivosCargar = Cargar(NOMBRE);

					Yo.setX(archivosCargar.posX);
					Yo.setY(archivosCargar.posY);
					Yo.setZ(archivosCargar.posZ);
					Yo.setDireccion(archivosCargar.dir);
					Inventario* mochilaYo = Yo.getInventario();
					mochilaYo->setLista(archivosCargar.listaYo);
					Yo.setInventarioCambiado(true);

					imprimirLista(archivosCargar.listaYo);
					mochilaYo->imprimirLista();
					Yo.imprimirInventario();
					
					scroll_times = archivosCargar.scroll_times;
					anguloCamaraVertical0 = archivosCargar.anguloCamaraVertical0;
					anguloCamaraHorizontal0 = archivosCargar.anguloCamaraHorizontal0;
					anguloCamaraHorizontal_1aPersona = archivosCargar.anguloCamaraHorizontal_1aPersona;
					anguloCamaraVertical_1aPersona = archivosCargar.anguloCamaraVertical_1aPersona;
					FASE_JUEGO = archivosCargar.FASE_JUEGO;
					FASE_JUEGOprev = archivosCargar.FASE_JUEGOprev;
					NOMBRE = archivosCargar.NOMBRE;
					tiempoCompletado = archivosCargar.tiempoCompletado;
					Sol.setTheta(archivosCargar.thetaSol);
					Sol.setPhi(archivosCargar.phiSol);

					ETAPA_GENERAL = JUGAR;
					break;
				}
				case REINICIAR_PARTIDA:
				{
						Yo.setPosition(YO_X0, YO_Y0, YO_Z0, YO_DIRECCION0*PI/180.0);
						Yo.vaciarInventario();
						scroll_times = SCROLL_TIMES0;
						ETAPA_GENERAL = JUGAR;
						FASE_JUEGO = "0";
						Sol.setPhi(ANG_PHI0*PI/180.0);
						Sol.setTheta(ANG_THETA0*PI/180.0);
						anguloCamaraVertical0 = 0;
						anguloCamaraHorizontal0 = 0;
						anguloCamaraVertical = 0;
						anguloCamaraHorizontal = 0;
						anguloCamaraVertical_1aPersona = 0;
						sentencia->setLabel(ajustarString(std::string(""), 30));
						toast_global.addMessageWithKey("Utiliza 'w' para caminar hacia adelante", 'w');
						toast_global.addMessageWithKey("Utiliza 's' para retroceder", 's');
						toast_global.addMessageWithKey("Utiliza 'a' para girar hacia la izquierda", 'a');
						toast_global.addMessageWithKey("Utiliza 'd' para girar hacia la derecha", 'd');
						anguloCamaraHorizontal_1aPersona = 0;
						break;
				}
				case ESTADISTICAS:
				{
						//obtener los nombres de las partidas guardadas y las fases del juego a las que llegaron
						std::vector<std::pair<std::string,int>> clasificacion = obtenerEstadisticas();
						
						//obtener el tamaño del nombre más largo para luego cuadrar los textos
						int maxSize = 0;
						for (std::vector<std::pair<std::string,int>>::iterator it = clasificacion.begin(); it != clasificacion.end(); ++it) {
							std::string nombre = (*it).first;
							if (nombre.size() > maxSize)
								maxSize = nombre.size();
						}

						//asignar los nombres y fases a las label de boxEstadisticas
						int n = 1;
						for (std::vector<std::pair<std::string,int>>::iterator it = clasificacion.begin(); it!=clasificacion.end() && n<=10; ++it) {
							std::string nombre = (*it).first;
							int fase = (*it).second;
							std::string labelText = std::to_string(n);
							for(int i=0; i<5; i++)
								labelText += "_";
							labelText += nombre;
							for(int i=nombre.size(); i<(maxSize+45); i++)
								labelText += "_";
							labelText += std::to_string(fase);

							osg::ref_ptr<RespuestaLabel> label = vectorEstadisticas.at(10-n);
							label->setLabel(labelText);
							n++;
						}
						std::string labelText = "N";
						for (int i = 0; i<5; i++)
							labelText += "_";
						labelText += "NOMBRE";
						for (int i = labelText.size(); i<(maxSize+45); i++)
							labelText += "_";
						labelText += "FASE";
						osg::ref_ptr<RespuestaLabel> label = vectorEstadisticas.at(10);
						label->setLabel(labelText);


						osg::ref_ptr<osgWidget::MouseHandler> estadisticasMouseHandler = new osgWidget::MouseHandler(wmEstadisticas);
						osg::ref_ptr<osgWidget::ResizeHandler> estadisticasResizeHandler = new osgWidget::ResizeHandler(wmEstadisticas, camera_estadisticas);
						viewer.addEventHandler(estadisticasMouseHandler);
						viewer.addEventHandler(estadisticasResizeHandler);
						boxEstadisticas->resize();
						wmEstadisticas->resizeAllWindows();
						viewer.setSceneData(grupoEstadisticas);
						viewer.realize();
						while (ETAPA_GENERAL==ESTADISTICAS && !viewer.done()) {
							if (labelVolver->clicado) {
								ETAPA_GENERAL = MENU_INICIO;
								//boxSonido->resize();
								labelVolver->reiniciarClick();
							}

							Sleep(10);
							viewer.frame();
						}
						//viewer.setDone(false);
						viewer.removeEventHandler(estadisticasResizeHandler);
						viewer.removeEventHandler(estadisticasMouseHandler);
						break;

				}
				case SALIR:
				{
						exit(0);
						break;
				}
				case MAPA:
				{
						osg::ref_ptr<SimulacionKeyboardEvent> KeyboardEvent = new SimulacionKeyboardEvent();
						viewer.addEventHandler(KeyboardEvent);

						//viewer.setUpViewInWindow(screenLength*margin, screenHeight*margin, NUM_PUNTOS, NUM_PUNTOS, 0);
						viewer.setSceneData(ss);
						viewer.realize();
						viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(0, 0, 1100), //desde dónde miramos
							osg::Vec3d(0, 0, 0),										 //hacia dónde miramos
							osg::Vec3d(0, 1, 0));										 //vector vertical
						while (ETAPA_GENERAL == MAPA && !viewer.done()) {
							viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(0, 0, 1100-2*zoom), //desde dónde miramos
																		osg::Vec3d(0, 0, 0), //hacia dónde miramos
																		osg::Vec3d(0, 1, 0));
								
							Sleep(10);
							viewer.frame();
						}

						viewer.removeEventHandler(KeyboardEvent);

						break;
				}
				default:
				{
						printf("default del switch\n");
						break;
				}
			}
	}

	return 0;

	}

