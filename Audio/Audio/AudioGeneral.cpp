#include "stdafx.h"
#include "AudioGeneral.h"


AudioGeneral::AudioGeneral()
{
	error = 0;
	frecuencia = 22050;
	canales = 5;
	if(SDL_Init(SDL_INIT_AUDIO < 0))
		error = 1;
	/*int result = 0;
	int flags = MIX_INIT_MP3; 
	if (flags != (result = Mix_Init(flags))) {
		printf("no se pudo inicializar mixer (result: %d).\n", result);
		printf("Mix_Init error: %s\n", Mix_GetError());
	}*/
	if (Mix_OpenAudio(frecuencia, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
		error = 2;
}


AudioGeneral::~AudioGeneral()
{
	Mix_CloseAudio();
}